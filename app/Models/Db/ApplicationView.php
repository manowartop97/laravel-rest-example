<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 12/27/17
 * Time: 3:04 PM
 */

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;


/**
 * Class ApplicationView
 *
 * @property int $id
 * @property int $application_id
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Db\Application $application
 * @property-read \App\Models\Db\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationView whereApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationView whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationView whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationView whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationView whereUserId($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class ApplicationView extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_id',
        'user_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function application()
    {
        return $this->belongsTo(Application::class);
    }
}