<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ApplicationResponse
 *
 * @package App\Models\Db
 * @property int $id
 * @property int $application_id
 * @property int $user_id
 * @property int $price
 * @property string $price_type
 * @property boolean $selected
 * @property string $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Db\Application $application
 * @property-read \App\Models\Db\User $user
 * @property-read \App\Models\Db\ApplicationResponseItem[]|\Illuminate\Database\Eloquent\Collection $items
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponse whereApplicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponse whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponse whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponse whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponse wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponse wherePriceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponse whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponse whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponse whereSelected($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class ApplicationResponse extends Model
{
    /**
     *
     */
    const PRICE_TYPE_APPROX = 'approx';

    /**
     *
     */
    const PRICE_TYPE_FIXED = 'fixed';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_id',
        'user_id',
        'price',
        'price_type',
        'description',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function application()
    {
        return $this->belongsTo(Application::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(ApplicationResponseItem::class);
    }
}
