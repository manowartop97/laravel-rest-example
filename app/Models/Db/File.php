<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Class File
 *
 * @property int $id
 * @property int $object_id
 * @property string $object_type
 * @property string $type
 * @property string $name
 * @property Model $object
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\File whereObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\File whereObjectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\File whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\File whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\File whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\File whereUpdatedAt($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class File extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'object_id',
        'object_type',
        'type',
        'name',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function object()
    {
        return $this->morphTo();
    }
}