<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Confirm
 *
 * @property int $id
 * @property int $object_id
 * @property string $object_type
 * @property string $code
 * @property \Carbon\Carbon $expiration
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Confirm whereObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Confirm whereObjectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Confirm whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Confirm whereExpiration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Confirm whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Confirm whereUpdatedAt($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class Confirm extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'object_id',
        'object_type',
        'expiration',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'expiration',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function object()
    {
        return $this->morphTo();
    }
}