<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 12/27/17
 * Time: 3:04 PM
 */

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 *
 * @package App\Models\Db
 * @property int $id
 * @property string $name
 * @property int $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Db\CategoryType[]|\Illuminate\Database\Eloquent\Collection $types
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Category whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class Category extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function types()
    {
        return $this->hasMany(CategoryType::class);
    }
}