<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 12/27/17
 * Time: 3:04 PM
 */

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;


/**
 * Class Application
 *
 * @property int $id
 * @property int $user_id
 * @property int $category_type_id
 * @property int $brand_id
 * @property int $status
 * @property string $description
 * @property int $pickup
 * @property string|null $city
 * @property string|null $street
 * @property string|null $house
 * @property string|null $building
 * @property integer $viewsCount
 * @property integer $responsesCount
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Db\User $user
 * @property-read \App\Models\Db\Brand $brand
 * @property-read \App\Models\Db\User $executor
 * @property-read \App\Models\Db\categoryType $categoryType
 * @property-read \App\Models\Db\File[]|\Illuminate\Database\Eloquent\Collection $files
 * @property-read \App\Models\Db\ApplicationView[]|\Illuminate\Database\Eloquent\Collection $views
 * @property-read \App\Models\Db\ApplicationResponse[]|\Illuminate\Database\Eloquent\Collection $responses
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Application whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Application whereCategoryTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Application whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Application whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Application whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Application whereHouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Application whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Application wherePickup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Application whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Application whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Application whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Application whereUserId($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class Application extends Model
{
    /**
     *
     */
    const DISK = 'application';

    /**
     *
     */
    const STATUS_NEW = 0;

    /**
     *
     */
    const STATUS_PROCESS = 1;

    /**
     *
     */
    const STATUS_COMPLETE = 2;

    /**
     *
     */
    const STATUS_CANCEL = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_type_id',
        'brand_id',
        'status',
        'description',
        'city',
        'street',
        'house',
        'building',
        'pickup',
    ];

    /**
     * @var array
     */
    protected $withCount = [
        'views',
        'responses',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function views()
    {
        return $this->hasMany(ApplicationView::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function executor()
    {
        return $this->hasOne(ApplicationResponse::class)->where('selected', true);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function responses()
    {
        return $this->hasMany(ApplicationResponse::class);
    }

    /**
     * @return User|null
     */
    public function getExecutorAttribute()
    {
        return ($executor = $this->getRelationValue('executor')->first()) ? $executor->user : null;
    }

    /**
     * @return int
     */
    public function getViewsCountAttribute()
    {
        return $this->getAttributeFromArray('views_count') ?: 0;
    }

    /**
     * @return int
     */
    public function getResponsesCountAttribute()
    {
        return $this->getAttributeFromArray('responses_count') ?: 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categoryType()
    {
        return $this->belongsTo(CategoryType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function files()
    {
        return $this->morphMany(File::class, 'object');
    }
}