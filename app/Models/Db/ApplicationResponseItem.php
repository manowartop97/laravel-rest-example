<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Db\ApplicationResponseItem
 *
 * @property int $id
 * @property int $application_response_id
 * @property string $name
 * @property int $price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Db\ApplicationResponse $response
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponseItem whereApplicationResponseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponseItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponseItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponseItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponseItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\ApplicationResponseItem whereUpdatedAt($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class ApplicationResponseItem extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_response_id',
        'name',
        'price',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function response()
    {
        return $this->belongsTo(ApplicationResponse::class);
    }
}
