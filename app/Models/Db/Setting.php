<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Settings
 *
 * @property int $id
 * @property int|null $referral_owner_price
 * @property int|null $referral_price
 * @property int|null $application_response_price
 * @property float|null $wallet_conversion
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Setting whereApplicationResponsePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Setting whereReferralPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Setting whereReferralOwnerPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Setting whereWalletConversion($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class Setting extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;
}
