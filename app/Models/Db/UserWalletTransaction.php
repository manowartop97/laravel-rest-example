<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserWalletTransaction
 *
 * @property int $id
 * @property int $wallet_id
 * @property integer $object_id
 * @property string $object_type
 * @property int $value
 * @property Model $object
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Db\UserWallet $wallet
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserWalletTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserWalletTransaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserWalletTransaction whereObjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserWalletTransaction whereObjectType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserWalletTransaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserWalletTransaction whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserWalletTransaction whereWalletId($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class UserWalletTransaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'object_id',
        'object_type',
        'value',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function wallet()
    {
        return $this->belongsTo(UserWallet::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function object()
    {
        return $this->morphTo();
    }
}
