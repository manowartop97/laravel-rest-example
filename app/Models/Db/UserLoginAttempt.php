<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserLoginAttempt
 *
 * @property int $user_id
 * @property integer $count
 * @property \Carbon\Carbon $expiration
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Db\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserLoginAttempt whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserLoginAttempt whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserLoginAttempt whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserLoginAttempt whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserLoginAttempt whereExpiration($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class UserLoginAttempt extends Model
{
    /**
     * @const integer
     */
    const COUNT_ATTEMPTS = 10;

    /**
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'count',
        'expiration',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'expiration',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}