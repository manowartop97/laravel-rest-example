<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 12/27/17
 * Time: 3:04 PM
 */

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 *
 * @package App\Models\Db
 * @property int $id
 * @property string $name
 * @property int $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Brand whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Brand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Brand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Brand whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\Brand whereUpdatedAt($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class Brand extends Model
{

}