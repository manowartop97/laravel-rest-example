<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Profile
 *
 * @property int $user_id
 * @property string|null $email
 * @property string $name
 * @property string|null $lastname
 * @property string|null $middlename
 * @property string|null $gender
 * @property \Carbon\Carbon|null $birthday
 * @property string|null $about
 * @property string|null $city
 * @property string|null $street
 * @property string|null $house
 * @property string|null $building
 * @property string $referral
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Db\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereMiddlename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereAboutMaster($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereAboutService($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereHouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserProfile whereReferral($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class UserProfile extends Model
{
    /**
     * @const string
     */
    const GENDER_MALE   = 'male';

    /**
     * @const string
     */
    const GENDER_FEMALE = 'female';

    /**
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'name',
        'lastname',
        'middlename',
        'gender',
        'birthday',
        'about',
        'city',
        'street',
        'house',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'birthday',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}