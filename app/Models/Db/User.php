<?php

namespace App\Models\Db;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @property int $id
 * @property string $phone
 * @property boolean $verified
 * @property boolean $dispatch
 * @property boolean $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \App\Models\Db\UserProfile $profile
 * @property-read \App\Models\Db\UserWallet $wallet
 * @property-read \App\Models\Db\UserType[]|\Illuminate\Database\Eloquent\Collection $userTypes
 * @property-read \App\Models\Db\Confirm[]|\Illuminate\Database\Eloquent\Collection $confirms
 * @property-read \App\Models\Db\File[]|\Illuminate\Database\Eloquent\Collection $files
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\User whereVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\User whereDispatch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\User whereActive($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     *
     */
    const DISK = 'profile';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone',
        'verified',
        'dispatch',
        'active',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne(UserProfile::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function wallet()
    {
        return $this->hasOne(UserWallet::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userTypes()
    {
        return $this->hasMany(UserType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function confirms()
    {
        return $this->morphMany(Confirm::class, 'object');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function files()
    {
        return $this->morphMany(File::class, 'object');
    }

    /**
     * @return boolean
     */
    public function isExecutor()
    {
        return $this->isUserType([UserType::TYPE_MASTER, UserType::TYPE_SERVICE]);
    }

    /**
     * @return boolean
     */
    public function isMaster()
    {
        return $this->isUserType([UserType::TYPE_MASTER]);
    }

    /**
     * @return boolean
     */
    public function isService()
    {
        return $this->isUserType([UserType::TYPE_SERVICE]);
    }

    /**
     * @return boolean
     */
    public function isCustomer()
    {
        return $this->isUserType([UserType::TYPE_COSTUMER]);
    }

    /**
     * @param array $types
     *
     * @return bool
     */
    public function isUserType(array $types)
    {
        return $this->userTypes()
            ->whereIn('type', $types)
            ->exists();
    }
}
