<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 12/27/17
 * Time: 3:05 PM
 */

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CategoryType
 *
 * @package App\Models\Db
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property int $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Db\Category $category
 * @property-read \App\Models\Db\Brand[]|\Illuminate\Database\Eloquent\Collection $brands
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\CategoryType whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\CategoryType whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\CategoryType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\CategoryType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\CategoryType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\CategoryType whereUpdatedAt($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class CategoryType extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function brands()
    {
        return $this->belongsToMany(Brand::class);
    }
}