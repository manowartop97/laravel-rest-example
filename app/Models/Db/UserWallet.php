<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;


/**
 * Class UserWallet
 *
 * @property int $id
 * @property int $user_id
 * @property int $balance
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Db\UserWalletTransaction[] $transactions
 * @property-read \App\Models\Db\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserWallet whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserWallet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserWallet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserWallet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserWallet whereUserId($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class UserWallet extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'balance',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(UserWalletTransaction::class);
    }
}
