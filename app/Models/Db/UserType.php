<?php

namespace App\Models\Db;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserType
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property-read \App\Models\Db\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserType whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Db\UserType whereType($value)
 * @mixin \Eloquent
 *
 * @package App\Models\Db
 */
class UserType extends Model
{
    /**
     *
     */
    const TYPE_COSTUMER = 'customer';

    /**
     *
     */
    const TYPE_MASTER   = 'master';

    /**
     *
     */
    const TYPE_SERVICE  = 'service';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}