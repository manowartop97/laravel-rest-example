<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 1/15/18
 * Time: 3:38 PM
 */

namespace App\Observers;

use App\Models\Db\UserProfile;

/**
 * Class UserProfileObserver
 *
 * @package App\Observers
 */
class UserProfileObserver
{

    /**
     * @param UserProfile $profile
     */
    public function creating(UserProfile $profile)
    {
        $profile->referral = unique_random($profile->getTable(), 'referral');
    }
}