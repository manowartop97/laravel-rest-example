<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 1/15/18
 * Time: 3:38 PM
 */

namespace App\Observers;

use App\Models\Db\Confirm;

/**
 * Class UserProfileObserver
 *
 * @package App\Observers
 */
class ConfirmObserver
{
    /**
     * @param Confirm $confirm
     */
    public function creating(Confirm $confirm)
    {
        $confirm->code = unique_random($confirm->getTable(), 'code');
    }
}