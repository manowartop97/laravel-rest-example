<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 1/9/18
 * Time: 12:10 PM
 */

namespace App\Serializers;

use League\Fractal\Serializer\ArraySerializer as BaseArraySerializer;

/**
 * Class ArraySerializer
 *
 * @package App\Serializers
 */
class ArraySerializer extends BaseArraySerializer
{
    /**
     * Serialize a collection.
     *
     * @param string $resourceKey
     * @param array  $data
     *
     * @return array
     */
    public function collection($resourceKey, array $data)
    {
        if ($resourceKey === false) {
            return $data;
        }

        return [$resourceKey ?: 'data' => $data];
    }
}