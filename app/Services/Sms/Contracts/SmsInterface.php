<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 20.12.17
 * Time: 13:34
 */

namespace App\Services\Sms\Contracts;

/**
 * Interface SmsInterface
 * @package App\Services\Sms\Contracts
 */
interface SmsInterface
{
    /**
     * @param string $phone
     * @param string $text
     *
     * @return boolean
     */
    public function send($phone, $text);
}