<?php

namespace App\Admin;

use Encore\Admin\Form;
use Encore\Admin\Admin as BaseAdmin;

/**
 * Class Admin
 * @package App\Admin
 */
class Admin extends BaseAdmin
{
    /**
     *
     */
    const SNAKE_PROPERTY = 'snakeAttributes';

    /**
     * @param mixed $model
     * @param \Closure $callable
     *
     * @return Grid
     */
    public function grid($model, \Closure $callable)
    {
        return new Grid(
            $this->prepareModel($model, true), $callable
        );
    }

    /**
     * @param mixed $model
     * @param \Closure $callable
     *
     * @return Form
     */
    public function form($model, \Closure $callable)
    {
        return new Form(
            $this->prepareModel($model, false), $callable
        );
    }

    /**
     * @param mixed $model
     * @param bool $snake
     * @return mixed
     */
    protected function prepareModel($model, $snake)
    {
        $model = $this->getModel($model);
        $model::${self::SNAKE_PROPERTY} = $snake;

        return $model;
    }
}