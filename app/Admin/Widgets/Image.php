<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 15.01.18
 * Time: 13:06
 */

namespace App\Admin\Widgets;

use Illuminate\Support\Facades\Storage;

/**
 * Class Image
 * @package App\Admin\Widgets
 */
class Image
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $caption;

    /**
     * @var integer
     */
    protected $width;

    /**
     * @var integer
     */
    protected $height;

    /**
     * Image constructor.
     *
     * @param string $disk
     * @param string $name
     * @param string $caption
     * @param int $width
     * @param int $height
     */
    public function __construct($disk, $name, $caption, $width = 200, $height = 200)
    {
        $this->url = Storage::disk($disk)->url($name);

        $this->caption = $caption;
        $this->width   = $width;
        $this->height  = $height;
    }

    /**
     * @return string
     */
    protected function render()
    {
        return "
            <div class='col-md-3'>
                <div class='thumbnail'>
                    <image src='{$this->url}' width='{$this->width}' height='{$this->height}'>
                    <div class='caption text-center'>
                        <h4>" . ucfirst($this->caption) . '</h4>
                    </div>
                </div>
            </div>
        ';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
}