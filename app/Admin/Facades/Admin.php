<?php

namespace App\Admin\Facades;

use Encore\Admin\Facades\Admin as BaseAdmin;

/**
 * Class Admin
 * @package App\Admin\Facades
 * @mixin \Admin
 */
class Admin extends BaseAdmin
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Admin\Admin::class;
    }
}