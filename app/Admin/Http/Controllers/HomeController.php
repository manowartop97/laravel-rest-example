<?php

namespace App\Admin\Http\Controllers;

use App\Models\Db\User;
use Encore\Admin\Layout\Row;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\InfoBox;
use App\Http\Controllers\Controller;

/**
 * Class HomeController
 * @package App\Admin\Controllers
 */
class HomeController extends Controller
{
    /**
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('admin.fixer'));

            $content->row(function (Row $row) {

                $row->column(3, new InfoBox(
                        __('admin.users'), 'users', 'aqua', null, User::count())
                );
            });
        });
    }
}
