<?php

namespace App\Admin\Http\Controllers;

use Encore\Admin\Grid;
use Encore\Admin\Form;
use App\Models\Db\User;
use App\Models\Db\UserType;
use App\Admin\Facades\Admin;
use App\Admin\Widgets\Image;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

/**
 * Class UserController
 * @package App\Admin\Http\Controllers
 */
class UserController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('admin.users'));
            $content->description(__('admin.list'));

            $grid = $this->grid();

            $grid->filter(function (Grid\Filter $filter) {

                $filter->like('profile.name', 'Name');
                $filter->like('profile.lastname', 'Lastname');
                $filter->like('profile.middlename', 'Middlename');

                $filter->equal('phone', 'Phone');
                $filter->equal('profile.email', 'Email')->email();

                $filter->equal('userTypes.type', 'Type')
                    ->select([
                        UserType::TYPE_COSTUMER,
                        UserType::TYPE_MASTER,
                        UserType::TYPE_SERVICE,
                    ]);

                $filter->equal('active', 'Active')
                    ->radio([
                        __('admin.no'),
                        __('admin.yes')
                    ]);

                $filter->equal('verified', 'Verified')
                    ->radio([
                        __('admin.no'),
                        __('admin.yes')
                    ]);

                $filter->date('created_at', 'Created');
            });

            $content->body($grid);
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     *
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('admin.users.update'));
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(User::class, function (Grid $grid) {

            $grid->disableCreation();
            $grid->disableRowSelector();

            $grid->tools(function (Grid\Tools $tools) {
                $tools->disableBatchActions();
            });

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
            });

            $grid->column('id');
            $grid->column('profile.name', 'Name');
            $grid->column('profile.lastname', 'Lastname');
            $grid->column('profile.middlename', 'Middlename');
            $grid->column('phone', 'Phone');

            $grid->column('profile.email', 'Email');

            $grid->column('', 'Type')->display(function () {

                return collect($this->{'userTypes'})->pluck('type')->map(function ($type) {
                    return ucfirst($type);
                })->implode(',');
            });

            $grid->column('active', 'Active')->display(function ($val) {
                return __($val ? 'admin.yes' : 'admin.no');
            });

            $grid->column('verified', 'Verified')->display(function ($val) {
                return __($val ? 'admin.yes' : 'admin.no');
            });

            $grid->column('created_at', 'Created')->sortable();
        });
    }

    /**
     * @return Form
     */
    protected function form()
    {
        return Admin::form(User::class, function (Form $form) {

            $form->disableReset();

            $form->tab(__('admin.general'), function (Form $form) {

                $form->display('', 'Type')->with(function () {

                    return collect($this->{'userTypes'})->pluck('type')->map(function ($type) {
                        return ucfirst($type);
                    })->implode(',');
                });

                $form->display('phone', 'Phone');
                $form->display('created_at', 'Created');

                $form
                    ->switch('active', 'Active')
                    ->default(false);

                $form
                    ->switch('verified', 'Verified')
                    ->default(false);

            })->tab(__('admin.profile'), function (Form $form) {

                $form->display('profile.email', 'Email');
                $form->display('profile.name', 'Name');
                $form->display('profile.lastname', 'Lastname');
                $form->display('profile.middlename', 'Middlename');

                $form->display('profile.gender', 'Gender')->with(function ($gender) {
                    return $gender ?? null;
                });

                $form->display('profile.birthday', 'Birthday');
                $form->display('profile.city', 'City');
                $form->display('profile.street', 'Street');
                $form->display('profile.house', 'House');
                $form->display('profile.building', 'Building');
                $form->display('profile.about', 'About');
                $form->display('profile.referral', 'Referral');

            })->tab(__('admin.files'), function (Form $form) {

                $form->display('files', 'Files')->with(function ($files) {

                    return collect($files)->reduce(function ($carry, $file) {
                        return $carry . new Image(User::DISK, $file['name'], $file['type']);
                    });
                });

            });

        });
    }
}