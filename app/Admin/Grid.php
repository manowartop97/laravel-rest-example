<?php

namespace App\Admin;

use Encore\Admin\Grid as BaseGrid;
use Illuminate\Database\Eloquent\Relations\Relation;

/**
 * Class Grid
 *
 * @package App\Admin
 */
class Grid extends BaseGrid
{
    /**
     * Add column to Grid.
     *
     * @param string $name
     * @param string $label
     *
     * @return BaseGrid\Column
     */
    public function column($name, $label = '')
    {
        $relations = [];
        $field = $name;

        if (($fields = explode('.', $name)) && \count($fields) > 1) {

            $field = array_pop($fields);
            $eloquent = $this->model()->eloquent();

            foreach ($fields as $relation) {

                $eloquent = $eloquent->$relation();

                if (!($eloquent instanceof Relation)) {
                    break;
                }

                $eloquent = $eloquent->getModel();
                $relations[] = $relation;
            }
        }

        if (!$label) {
            $label = ucfirst($field);
        }

        if (!empty($relations)) {

            $field = implode('.', array_merge(
                  array_map(function ($relation) { return snake_case($relation); }, $relations), [$field])
            );
        }

        $column = $this->addColumn($field, $label);

        if (!empty($relations)) {

            $relation = implode('.', $relations);
            $this->model()->{'with'}($relation);

            $column->setRelation($relation, array_shift($relations));
        }

        return $column;
    }
}