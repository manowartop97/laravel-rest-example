<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 18.12.17
 * Time: 15:08
 */

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

/**
 * Class PhoneRule
 * @package App\Rules
 */
class PhoneRule implements Rule
{
    /**
     * @param string $attribute
     * @param mixed $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Validator::make([$attribute => $value], [
            $attribute => 'phone' . (app()->environment() === 'production' ? ':RU' : ':AUTO')
        ])->passes();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute has wrong format!';
    }
}