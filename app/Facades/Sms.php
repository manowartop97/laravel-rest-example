<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;
use App\Services\Sms\Contracts\SmsInterface;

/**
 * Class Sms
 * @package App\Facades
 */
class Sms extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return SmsInterface::class;
    }
}