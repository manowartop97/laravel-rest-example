<?php

namespace App\Events;

/**
 * Class ServiceRegisteredEvent
 * @package App\Events
 */
class ServiceRegisteredEvent extends CustomerRegisteredEvent
{
}