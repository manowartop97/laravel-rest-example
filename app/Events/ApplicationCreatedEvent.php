<?php

namespace App\Events;

use App\Models\Db\Application;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class ApplicationCreatedEvent
 * @package App\Events
 */
class ApplicationCreatedEvent
{
    use Dispatchable, SerializesModels;

    /**
     * @var Application
     */
    public $application;

    /**
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }
}