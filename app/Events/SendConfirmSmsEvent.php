<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class SendConfirmSmsEvent
 * @package App\Events
 */
class SendConfirmSmsEvent
{
    use Dispatchable, SerializesModels;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $code;

    /**
     * @param string $phone
     * @param string $code
     */
    public function __construct($phone, $code)
    {
        $this->phone = $phone;
        $this->code = $code;
    }
}