<?php

namespace App\Events;

/**
 * Class MasterRegisteredEvent
 * @package App\Events
 */
class MasterRegisteredEvent extends CustomerRegisteredEvent
{
}