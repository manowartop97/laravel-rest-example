<?php

namespace App\Events;

use App\Models\Db\ApplicationResponse;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class ApplicationResponseCreatedEvent
 *
 * @package App\Events
 */
class ApplicationResponseCreatedEvent
{
    use Dispatchable, SerializesModels;

    /**
     * @var ApplicationResponse
     */
    public $response;

    /**
     * @param ApplicationResponse $response
     */
    public function __construct(ApplicationResponse $response)
    {
        $this->response = $response;
    }
}