<?php

namespace App\Events;

use App\Models\Db\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class CustomerRegisteredEvent
 * @package App\Events
 */
class CustomerRegisteredEvent
{
    use Dispatchable, SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}