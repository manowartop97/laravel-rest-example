<?php

namespace App\Providers;

use Encore\Admin\Console\MakeCommand;
use Illuminate\Support\ServiceProvider;
use App\Console\Commands\AdminMakeCommand;

/**
 * Class AdminServiceProvider
 * @package App\Providers
 */
class AdminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        app('view')->prependNamespace('admin', resource_path('views/admin'));

        $this->app->extend(MakeCommand::class, function () {
            return resolve(AdminMakeCommand::class);
        });
    }
}
