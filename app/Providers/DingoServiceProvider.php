<?php

namespace App\Providers;

use Dingo\Api\Exception\Handler;
use Illuminate\Auth\Access\AuthorizationException;
use League\Fractal\Manager;
use Dingo\Api\Transformer\Factory;
use Illuminate\Support\ServiceProvider;
use Dingo\Api\Transformer\Adapter\Fractal;
use App\Serializers\ArraySerializer;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class ApiServiceProvider
 *
 * @package App\Providers
 */
class DingoServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function boot()
    {
        $this->changeSerializer();
        $this->resisterHandler();
    }

    /**
     *
     */
    protected function changeSerializer()
    {
        $this->app->get(Factory::class)->setAdapter(function () {
            return new Fractal((new Manager())->setSerializer(new ArraySerializer()));
        });
    }

    /**
     *
     */
    protected function resisterHandler()
    {
        $this->app->get(Handler::class)->register(function (AuthorizationException $e) {
            throw new AccessDeniedHttpException(null, $e);
        });
    }
}
