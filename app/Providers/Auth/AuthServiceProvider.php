<?php

namespace App\Providers\Auth;

use App\Models\Db\Application;
use App\Models\Db\ApplicationResponse;
use App\Models\Db\User;
use App\Policies\ApplicationPolicy;
use App\Policies\ApplicationResponsePolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

/**
 * Class AuthServiceProvider
 *
 * @package App\Providers
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Application::class => ApplicationPolicy::class,
        ApplicationResponse::class => ApplicationResponsePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
