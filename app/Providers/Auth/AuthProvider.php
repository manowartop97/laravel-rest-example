<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 1/2/18
 * Time: 10:34 AM
 */

namespace App\Providers\Auth;

use App\Models\Db\User;
use Tymon\JWTAuth\Providers\Auth\Illuminate;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class Illuminate
 *
 * @package App\Providers\Auth
 */
class AuthProvider extends Illuminate
{
    /**
     * @return User|null
     */
    public function user()
    {
        /**@var User $user*/
        if ($user = parent::user()) {

            if (!$user->active) {
                throw new AccessDeniedHttpException('User disabled');
            }

            return $user;
        }

        return null;
    }
}