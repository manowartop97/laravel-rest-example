<?php

namespace App\Providers;

use App\Models\Db\Confirm;
use App\Models\Db\UserProfile;
use App\Observers\ConfirmObserver;
use App\Observers\UserProfileObserver;
use Illuminate\Support\ServiceProvider;

/**
 * Class ObserveServiceProvider
 *
 * @package App\Providers
 */
class ObserveServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function boot()
    {
        Confirm::observe(ConfirmObserver::class);
        UserProfile::observe(UserProfileObserver::class);
    }

}
