<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 20.12.17
 * Time: 13:58
 */

namespace App\Providers;

use App\Services\Sms\Sms;
use Illuminate\Support\ServiceProvider;
use App\Services\Sms\Contracts\SmsInterface;

/**
 * Class SmsServiceProvider
 * @package App\Providers
 */
class SmsServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SmsInterface::class, Sms::class);
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [SmsInterface::class];
    }
}