<?php

namespace App\Providers;

use App\Models\Db\Application;
use App\Models\Db\ApplicationResponse;
use App\Models\Db\User;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

/**
 * Class PolymorphicServiceProvider
 * @package App\Providers
 */
class PolymorphicServiceProvider extends ServiceProvider
{
    /**
     *@return void
     */
    public function boot()
    {
        Relation::morphMap([
            'user' => User::class,
            'application' => Application::class,
            'application-response' => ApplicationResponse::class,
        ]);
    }
}