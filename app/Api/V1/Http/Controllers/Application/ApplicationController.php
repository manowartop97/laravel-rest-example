<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 25.12.17
 * Time: 14:35
 */

namespace App\Api\V1\Http\Controllers\Application;

use App\Api\V1\Http\Controllers\Controller;
use App\Api\V1\Http\Requests\Application\ApplicationCreateRequest;
use App\Api\V1\Http\Requests\Application\ApplicationStatusRequest;
use App\Api\V1\Http\Requests\Application\ApplicationUpdateRequest;
use App\Repositories\Application\ApplicationRepository;
use App\Transformers\ApplicationTransformer;
use App\Models\Db\Application;

/**
 * Class ApplicationController
 *
 * @SWG\Definition(
 *    definition="Application",
 *    type="object",
 *    @SWG\Property(property="id", type="integer"),
 *    @SWG\Property(property="category", type="integer"),
 *    @SWG\Property(property="brand", type="integer"),
 *    @SWG\Property(property="status", type="integer"),
 *    @SWG\Property(property="description", type="string"),
 *    @SWG\Property(property="pickup", type="boolean"),
 *    @SWG\Property(property="city", type="string"),
 *    @SWG\Property(property="street", type="string"),
 *    @SWG\Property(property="house", type="string"),
 *    @SWG\Property(property="building", type="string"),
 *    @SWG\Property(property="viewsCount", type="integer"),
 *    @SWG\Property(property="responsesCount", type="integer")
 * )
 *
 * @package App\Api\V1\Http\Controllers
 */
class ApplicationController extends Controller
{
    /**
     * @var ApplicationRepository
     */
    protected $application;

    /**
     * UserController constructor.
     *
     * @param ApplicationRepository $application
     */
    public function __construct(ApplicationRepository $application)
    {
        $this->middleware('api.auth');

        $this->application = $application;
    }

    /**
     * @SWG\Get(
     *    path = "/api/applications/{id}/show",
     *    tags={"Application"},
     *    description="Get application by id",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="id",
     *       in="path",
     *       type="integer",
     *       required=true,
     *       description="An application id"
     *    ),
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/ApplicationShow")
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    @SWG\Response(
     *       response = 403,
     *       description = "Forbidden",
     *    ),
     *    @SWG\Response(
     *       response = 404,
     *       description = "Not found!",
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * ),
     *
     * @SWG\Definition(
     *    definition="ApplicationShow",
     *    allOf={
     *       @SWG\Schema(type="object", ref="#/definitions/Application"),
     *    },
     *    @SWG\Property(property="files",ref="#/definitions/Files"),
     *    @SWG\Property(property="responses", type="array", @SWG\Items(ref="#/definitions/ApplicationResponses"))
     * )
     *
     * @param integer $id
     *
     * @return \Dingo\Api\Http\Response
     */
    public function get($id)
    {
        if (!($application = $this->application->getById($id))) {
            $this->response->errorNotFound();
        }

        $this->authorize('show', $application);

        $this->application->addView($application, $this->auth->user());

        return $this->response->item(
            $application, (new ApplicationTransformer())->withIncludes()
        );
    }

    /**
     * @SWG\Get(
     *    path = "/api/applications",
     *    tags={"Application"},
     *    description="Get applications list",
     *    produces={"application/json"},
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/Applications")
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * ),
     *
     * @SWG\Definition(
     *    definition="Applications",
     *    type="object",
     *    @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/Application")),
     *    @SWG\Property(property="meta", ref="#/definitions/Meta")
     * )
     *
     * @return \Dingo\Api\Http\Response
     */
    public function getAll()
    {
        return $this->response->paginator(
            $this->application->getAll($this->auth->user()), new ApplicationTransformer()
        );
    }

    /**
     * @SWG\Post(
     *    path = "/api/applications/create",
     *    tags={"Application"},
     *    description="Create an application",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/ApplicationCreateRequest")
     *    ),
     *    @SWG\Response(
     *       response = 201,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/Application")
     *    ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Cannot create an application!"
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    @SWG\Response(
     *       response = 403,
     *       description = "Forbidden"
     *    ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * ),
     *
     * @SWG\Definition(
     *    definition="ApplicationCreateRequest",
     *    type="object",
     *    @SWG\Property(property="category_type_id", type="integer"),
     *    @SWG\Property(property="brand_id", type="integer"),
     *    @SWG\Property(property="description", type="string"),
     *    @SWG\Property(property="photo", type="array", @SWG\Items(type="file")),
     *    @SWG\Property(property="pickup", type="boolean"),
     *    @SWG\Property(property="city", type="string"),
     *    @SWG\Property(property="street", type="string"),
     *    @SWG\Property(property="house", type="string"),
     *    @SWG\Property(property="building", type="string")
     * )
     *
     * @param ApplicationCreateRequest $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function create(ApplicationCreateRequest $request)
    {
        $this->authorize('create', Application::class);

        if (!($application = $this->application->create($this->auth->user(), $request->validated()))) {
            $this->response->error('Cannot create an application!', 202);
        }

        return $this->response->item($application, new ApplicationTransformer())
            ->setStatusCode(201);
    }

    /**
     * @SWG\Put(
     *    path = "/api/applications/{id}/update",
     *    tags={"Application"},
     *    description="Update an application",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/ApplicationUpdateRequest"),
     *    ),
     *    @SWG\Parameter(
     *       name="id",
     *       in="path",
     *       type="integer",
     *       required=true,
     *       description="An application id"
     *    ),
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/Application")
     *    ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Cannot update an application!",
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    @SWG\Response(
     *       response = 403,
     *       description = "Forbidden",
     *    ),
     *    @SWG\Response(
     *       response = 404,
     *       description = "Not Found",
     *    ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * ),
     *
     * @SWG\Definition(
     *    definition="ApplicationUpdateRequest",
     *    allOf={
     *       @SWG\Schema(type="object", ref="#/definitions/ApplicationCreateRequest"),
     *    },
     * ),
     *
     * @param ApplicationUpdateRequest $request
     * @param integer $id
     *
     * @return \Dingo\Api\Http\Response
     */
    public function update(ApplicationUpdateRequest $request, $id)
    {
        if (!($application = $this->application->getById($id))) {
            $this->response->errorNotFound();
        }

        $this->authorize('update', $application);

        if (!($application = $this->application->update($application, $request->validated(), $request->getDeletedFiles()))) {
            $this->response->error('Cannot update an application!', 202);
        }

        return $this->response->item(
            $application, new ApplicationTransformer()
        );
    }

    /**
     * @SWG\Put(
     *    path = "/api/applications/{id}/change-status",
     *    tags={"Application"},
     *    description="Change application status",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/ApplicationStatusRequest"),
     *    ),
     *    @SWG\Parameter(
     *       name="id",
     *       in="path",
     *       type="integer",
     *       required=true,
     *       description="An application id"
     *    ),
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/Application")
     *    ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Cannot update an application status!",
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    @SWG\Response(
     *       response = 403,
     *       description = "Forbidden",
     *    ),
     *    @SWG\Response(
     *       response = 404,
     *       description = "Not Found",
     *    ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * )
     *
     * @SWG\Definition(
     *    definition="ApplicationStatusRequest",
     *    type="object",
     *    @SWG\Property(property="status", type="integer", enum={2, 3}, description="2 - complete, 3 - cancel")
     * )
     *
     * @param ApplicationStatusRequest $request
     * @param integer $id
     *
     * @return \Dingo\Api\Http\Response
     */
    public function changeStatus(ApplicationStatusRequest $request, $id)
    {
        if (!($application = $this->application->getById($id))) {
            $this->response->errorNotFound();
        }

        $this->authorize('changeStatus', [$application, $request->getStatus()]);

        if (!($application = $this->application->update($application, $request->validated()))) {
            $this->response->error('Cannot update an application status!', 202);
        }

        return $this->response->item(
            $application, new ApplicationTransformer()
        );
    }
}