<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 25.12.17
 * Time: 14:35
 */

namespace App\Api\V1\Http\Controllers\Application;

use App\Api\V1\Http\Controllers\Controller;
use App\Api\V1\Http\Requests\Application\ApplicationResponseCreateRequest;
use App\Api\V1\Http\Requests\Application\ApplicationResponseUpdateRequest;
use App\Models\Db\ApplicationResponse;
use App\Repositories\Application\ApplicationRepository;
use App\Repositories\Application\ApplicationResponseRepository;
use App\Transformers\ApplicationResponseTransformer;

/**
 * Class ApplicationController
 *
 * @SWG\Definition(
 *    definition="ApplicationResponses",
 *    type="object",
 *    @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/ApplicationResponse")),
 *    @SWG\Property(property="meta", ref="#/definitions/Meta")
 * ),
 *
 * @SWG\Definition(
 *    definition="ApplicationResponse",
 *    type="object",
 *    @SWG\Property(property="id", type="integer"),
 *    @SWG\Property(property="price", type="integer"),
 *    @SWG\Property(property="price_type", type="string", enum={"approx", "fixed"}),
 *    @SWG\Property(property="selected", type="boolean"),
 *    @SWG\Property(property="description", type="string"),
 *    @SWG\Property(property="items", type="array", @SWG\Items(ref="#/definitions/ApplicationResponseItems"))
 * ),
 *
 * @SWG\Definition(
 *    definition="ApplicationResponseItems",
 *    type="object",
 *    @SWG\Property(property="id", type="integer"),
 *    @SWG\Property(property="name", type="string"),
 *    @SWG\Property(property="price", type="integer")
 * ),
 *
 * @SWG\Definition(
 *    definition="ApplicationResponseRequest",
 *    type="object",
 *    @SWG\Property(property="application_id", type="integer"),
 *    @SWG\Property(property="price", type="integer"),
 *    @SWG\Property(property="price_type", type="string", enum={"approx", "fixed"}),
 *    @SWG\Property(property="description", type="string")
 * )
 *
 * @package App\Api\V1\Http\Controllers
 */
class ApplicationResponseController extends Controller
{
    /**
     * @var ApplicationRepository
     */
    protected $application;

    /**
     * @var ApplicationResponseRepository
     */
    protected $applicationResponse;

    /**
     * UserController constructor.
     *
     * @param ApplicationRepository $application
     * @param ApplicationResponseRepository $applicationResponse
     */
    public function __construct(ApplicationRepository $application, ApplicationResponseRepository $applicationResponse)
    {
        $this->middleware('api.auth');

        $this->application = $application;
        $this->applicationResponse = $applicationResponse;
    }

    /**
     * @SWG\Post(
     *    path = "/api/applications/responses/{id}/create",
     *    tags={"Application"},
     *    description="Create an application response",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/ApplicationResponseCreateRequest"),
     *    ),
     *    @SWG\Parameter(
     *       name="id",
     *       in="path",
     *       type="integer",
     *       required=true,
     *       description="An application id"
     *    ),
     *    @SWG\Response(
     *       response = 201,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/ApplicationResponse")
     *    ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Cannot create an application response!",
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    @SWG\Response(
     *       response = 402,
     *       description = "Not enough money",
     *    ),
     *    @SWG\Response(
     *       response = 403,
     *       description = "Forbidden",
     *    ),
     *    @SWG\Response(
     *       response = 404,
     *       description = "Not Found",
     *    ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * ),
     *
     * @SWG\Definition(
     *    definition="ApplicationResponseCreateRequest",
     *    allOf={
     *       @SWG\Schema(type="object", ref="#/definitions/ApplicationResponseRequest"),
     *    },
     *    @SWG\Property(property="items", type="array", @SWG\Items(ref="#/definitions/ItemsCreateRequest"))
     * ),
     *
     * @SWG\Definition(
     *    definition="ItemsCreateRequest",
     *    type="object",
     *    @SWG\Property(property="price", type="integer"),
     *    @SWG\Property(property="name", type="string")
     * )
     *
     * @param ApplicationResponseCreateRequest $request
     * @param integer $id application id
     *
     * @return \Dingo\Api\Http\Response
     */
    public function create(ApplicationResponseCreateRequest $request, $id)
    {
        if (!($application = $this->application->getById($id))) {
            $this->response->errorNotFound();
        }

        $this->authorize('create', [ApplicationResponse::class, $application]);

        if (!($response = $this->applicationResponse->create($this->auth->user(), $application, $request->validated(), $request->getItems()))) {
            $this->response->error('Cannot create an application response!', 202);
        }

        return $this->response->item($response, new ApplicationResponseTransformer())
            ->setStatusCode(201);
    }

    /**
     * @SWG\Put(
     *    path = "/api/applications/responses/{id}/update",
     *    tags={"Application"},
     *    description="Create an application response",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/ApplicationResponseUpdateRequest"),
     *    ),
     *    @SWG\Parameter(
     *       name="id",
     *       in="path",
     *       type="integer",
     *       required=true,
     *       description="An application response id"
     *    ),
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/ApplicationResponse")
     *    ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Cannot update an application response!",
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    @SWG\Response(
     *       response = 403,
     *       description = "Forbidden",
     *    ),
     *    @SWG\Response(
     *       response = 404,
     *       description = "Not Found",
     *    ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * ),
     *
     * @SWG\Definition(
     *    definition="ApplicationResponseUpdateRequest",
     *    allOf={
     *       @SWG\Schema(type="object", ref="#/definitions/ApplicationResponseRequest"),
     *    },
     *    @SWG\Property(property="items", type="array", @SWG\Items(ref="#/definitions/ItemsUpdateRequest"))
     * ),
     *
     * @SWG\Definition(
     *    definition="ItemsUpdateRequest",
     *    allOf={
     *       @SWG\Schema(type="object", ref="#/definitions/ItemsCreateRequest"),
     *    },
     *    @SWG\Property(property="id", type="integer")
     * )
     *
     * @param ApplicationResponseUpdateRequest $request
     * @param integer $id application response id
     *
     * @return \Dingo\Api\Http\Response
     */
    public function update(ApplicationResponseUpdateRequest $request, $id)
    {
        if (!($response = $this->applicationResponse->getById($id))) {
            $this->response->errorNotFound();
        }

        $this->authorize('update', $response);

        if (!($response = $this->applicationResponse->update($response, $request->validated(), $request->getItems()))) {
            $this->response->error('Cannot update an application response!', 202);
        }

        return $this->response->item($response, new ApplicationResponseTransformer());
    }


    /**
     * @SWG\Put(
     *    path = "/api/applications/responses/{id}/confirm",
     *    tags={"Application"},
     *    description="Confirm an application response",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="id",
     *       in="path",
     *       type="integer",
     *       required=true,
     *       description="An application response id"
     *    ),
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/ApplicationResponse")
     *    ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Cannot update an application response!",
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    @SWG\Response(
     *       response = 403,
     *       description = "Forbidden",
     *    ),
     *    @SWG\Response(
     *       response = 404,
     *       description = "Not Found",
     *    ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * ),
     *
     * @param integer $id response id
     *
     * @return \Dingo\Api\Http\Response
     */
    public function confirm($id)
    {
        if (!($response = $this->applicationResponse->getById($id))) {
            $this->response->errorNotFound();
        }

        $this->authorize('confirm', $response);

        if (!($response = $this->applicationResponse->confirm($response))) {
            $this->response->error('Cannot update an application response!', 202);
        }

        return $this->response->item($response, new ApplicationResponseTransformer());
    }
}