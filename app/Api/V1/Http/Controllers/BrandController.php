<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 1/9/18
 * Time: 11:51 AM
 */

namespace App\Api\V1\Http\Controllers;

use App\Repositories\Brand\BrandRepository;
use App\Transformers\BrandTransformer;
use Illuminate\Http\Request;

/**
 * Class BrandController
 *
 * @package App\Api\V1\Http\Controllers
 */
class BrandController extends Controller
{
    /**
     * @var BrandRepository
     */
    protected $brand;

    /**
     * BrandController constructor.
     *
     * @param BrandRepository $brand
     */
    public function __construct(BrandRepository $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @SWG\Get(
     *    path = "/api/brands",
     *    tags={"Brand"},
     *    description="Get brands list",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="search",
     *       in="query",
     *       type="string",
     *       description = "A brand name for search"
     *    ),
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/Brands")
     *    ),
     * ),
     *
     * @SWG\Definition(
     *    definition="Brands",
     *    type="object",
     *    @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/Brand")),
     * ),
     *
     * @SWG\Definition(
     *    definition="Brand",
     *    @SWG\Property(property="id", type="integer"),
     *    @SWG\Property(property="name", type="string")
     * )
     *
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function getAll(Request $request)
    {
        return $this->response->collection(
            $this->brand->getAll($request->get('search')), new BrandTransformer()
        );
    }
}