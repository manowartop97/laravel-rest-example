<?php

namespace App\Api\V1\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller as BaseController;

/**
 * Class Controller
 *
 * @SWG\Swagger(
 *    basePath="/",
 *    @SWG\Info(
 *       version="1.0",
 *       title="Fixer API",
 *       description="The Fixer system is a platform where you can leave a request for the fixing of your technique.",
 *       termsOfService="",
 *    ),
 *
 *   @SWG\Definition(
 *      definition="ValidationErrors",
 *      type="object",
 *      @SWG\Property(property="message", type="string"),
 *      @SWG\Property(property="errors", type="object",
 *        @SWG\Property(property="field", type="array",
 *           @SWG\Items(type="string")
 *        ),
 *      ),
 *      @SWG\Property(property="status_code", type="integer"),
 *   ),
 *
 *   @SWG\Definition(
 *      definition="Error",
 *      type="object",
 *      @SWG\Property(property="message", type="string"),
 *      @SWG\Property(property="status_code", type="integer"),
 *   ),
 *
 *   @SWG\Definition(
 *      definition="Meta",
 *      type="object",
 *      @SWG\Property(property="pagination", ref="#/definitions/Pagination"),
 *   ),
 *
 *   @SWG\Definition(
 *      definition="Pagination",
 *      type="object",
 *      @SWG\Property(property="total", type="integer"),
 *      @SWG\Property(property="count", type="integer"),
 *      @SWG\Property(property="per_page", type="integer"),
 *      @SWG\Property(property="current_page", type="integer"),
 *      @SWG\Property(property="total_pages", type="integer"),
 *      @SWG\Property(property="links", type="array", @SWG\Items(type="string")),
 *   ),
 *
 *   @SWG\Definition(
 *      definition="Files",
 *      type="array",
 *      @SWG\Items(ref="#/definitions/File"),
 *   ),
 *
 *   @SWG\Definition(
 *      definition="File",
 *      type="object",
 *      @SWG\Property(property="id", type="integer"),
 *      @SWG\Property(property="type", type="string"),
 *      @SWG\Property(property="url", type="string")
 *   )
 * )
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use Helpers;
}
