<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 1/9/18
 * Time: 11:51 AM
 */

namespace App\Api\V1\Http\Controllers;

use App\Repositories\Category\CategoryRepository;
use App\Transformers\CategoryTransformer;
use Illuminate\Http\Request;

/**
 * Class BrandController
 *
 * @package App\Api\V1\Http\Controllers
 */
class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    protected $category;

    /**
     * CategoryController constructor.
     *
     * @param CategoryRepository $category
     */
    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
    }

    /**
     * @SWG\Get(
     *    path = "/api/categories",
     *    tags={"Category"},
     *    description="Get categories list",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="search",
     *       in="query",
     *       type="string",
     *    ),
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/Categories")
     *    ),
     * ),
     *
     * @SWG\Definition(
     *    definition="Categories",
     *    type="object",
     *    @SWG\Property(property="data", type="array", @SWG\Items(ref="#/definitions/Category"))
     * ),
     *
     * @SWG\Definition(
     *    definition="Category",
     *    @SWG\Property(property="id", type="integer"),
     *    @SWG\Property(property="name", type="string"),
     *    @SWG\Property(property="types", type="array",
     *       @SWG\Items(type="object",
     *          @SWG\Property(property="id", type="integer"),
     *          @SWG\Property(property="name", type="string"),
     *          @SWG\Property(property="brands", type="array",@SWG\Items(ref="#/definitions/Brand"))
     *       )
     *    )
     * )
     *
     * @param Request $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function getAll(Request $request)
    {
        return $this->response->collection(
            $this->category->getAll($request->get('search')), new CategoryTransformer()
        );
    }
}