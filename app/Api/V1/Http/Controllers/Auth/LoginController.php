<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 22.12.17
 * Time: 10:40
 */

namespace App\Api\V1\Http\Controllers\Auth;

use App\Api\V1\Http\Controllers\Controller;
use App\Repositories\Confirm\ConfirmRepository;
use App\Repositories\User\UserRepository;
use App\Api\V1\Http\Requests\Auth\LoginRequest;
use App\Api\V1\Http\Requests\Auth\LoginCodeRequest;
use App\Repositories\User\LoginAttemptRepository;

/**
 * Class LoginController
 * @package App\Api\V1\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * @var LoginAttemptRepository
     */
    protected $loginAttempt;

    /**
     * @var ConfirmRepository
     */
    protected $confirm;

    /**
     * @param UserRepository $user
     * @param LoginAttemptRepository $attempt
     * @param ConfirmRepository $confirm
     */
    public function __construct(UserRepository $user, LoginAttemptRepository $attempt, ConfirmRepository $confirm)
    {
        $this->middleware('api.auth')->only('logout');

        $this->user = $user;
        $this->confirm = $confirm;
        $this->loginAttempt = $attempt;
    }

    /**
     * @SWG\Post(
     *    path = "/api/login/send-code",
     *    tags={"Authentication"},
     *    description="Send confirm code",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/LoginCodeRequest"),
     *    ),
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *    ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Cannot send a confirm code!",
     *   ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     * ),
     *
     * @SWG\Definition(
     *    definition="LoginCodeRequest",
     *    type="object",
     *    @SWG\Property(property="phone", type="string")
     * )
     *
     * @param LoginCodeRequest $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function sendCode(LoginCodeRequest $request)
    {
        if (!($user = $this->user->getByPhone($request->getPhone())) || !$this->confirm->create($user)) {
            $this->response->error('Cannot send a confirm code!', 202);
        }

        return $this->response()->noContent()
            ->setStatusCode(200);
    }

    /**
     * @SWG\Post(
     *    path = "/api/login",
     *    tags={"Authentication"},
     *    description="Sing in",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/LoginRequest"),
     *    ),
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/Token")
     *    ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Unable to login!",
     *    ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     * ),
     *
     * @SWG\Definition(
     *    definition="LoginRequest",
     *    type="object",
     *    @SWG\Property(property="phone", type="string"),
     *    @SWG\Property(property="code", type="string")
     * ),
     *
     * @SWG\Definition(
     *    definition="Token",
     *    type="object",
     *    @SWG\Property(property="token", type="string")
     * )
     *
     * @param LoginRequest $request
     *
     * @return array
     */
    public function login(LoginRequest $request)
    {
        if (!($user = $this->user->getByPhone($request->getPhone())) || !$this->loginAttempt->check($user) || !$this->confirm->isValid($user, $request->getCode())) {
            $this->response->error('Unable to login!', 202);
        }

        $this->confirm->invalidate($user, $request->getCode());

        return [
            'token' => \JWTAuth::fromUser($user)
        ];
    }

    /**
     * @SWG\Post(
     *    path = "/api/logout",
     *    tags={"Authentication"},
     *    description="Logout",
     *    produces={"application/json"},
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success"
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * )
     *
     * @return \Dingo\Api\Http\Response
     */
    public function logout()
    {
        \JWTAuth::invalidate(\JWTAuth::getToken());

        return $this->response()->noContent()
            ->setStatusCode(200);
    }
}