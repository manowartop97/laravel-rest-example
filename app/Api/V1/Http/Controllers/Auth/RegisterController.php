<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 18.12.17
 * Time: 14:53
 */

namespace App\Api\V1\Http\Controllers\Auth;

use App\Models\Db\UserType;
use App\Api\V1\Http\Controllers\Controller;
use App\Transformers\UserTransformer;
use App\Repositories\User\UserRepository;
use App\Api\V1\Http\Requests\Auth\MasterRequest;
use App\Api\V1\Http\Requests\Auth\ServiceRequest;
use App\Api\V1\Http\Requests\Auth\CustomerRequest;

/**
 * Class RegisterController
 *
 * @package App\Api\V1\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * @SWG\Post(
     *    path = "/api/register/customer",
     *    tags={"User"},
     *    description="A customer sign up",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/CustomerRequest"),
     *     ),
     *     @SWG\Response(
     *        response = 201,
     *        description = "Success",
     *        @SWG\Schema(ref="#/definitions/User")
     *     ),
     *     @SWG\Response(
     *        response = 202,
     *        description = "Cannot register a customer!",
     *     ),
     *     @SWG\Response(
     *        response = 422,
     *        description = "Validation errors",
     *        @SWG\Schema(ref="#/definitions/ValidationErrors")
     *     ),
     * ),
     *
     * @SWG\Definition(
     *    definition="CustomerRequest",
     *    type="object",
     *    @SWG\Property(property="phone", type="string"),
     *    @SWG\Property(property="name", type="string")
     * )
     *
     * @param CustomerRequest $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function registerCustomer(CustomerRequest $request)
    {
        if (!($user = $this->user->create($request->validated(), UserType::TYPE_COSTUMER))) {
            $this->response->error('Cannot register a customer!', 202);
        }

        return $this->response->item($user, new UserTransformer())
            ->setStatusCode(201);
    }

    /**
     * @SWG\Post(
     *    path = "/api/register/master",
     *    tags={"User"},
     *    description="A master sign up",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/MasterRequest"),
     *    ),
     *    @SWG\Response(
     *       response = 201,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/User")
     *    ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Cannot register an executor master!",
     *    ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     * ),
     *
     * @SWG\Definition(
     *    definition="MasterRequest",
     *    type="object",
     *    @SWG\Property(property="phone", type="string"),
     *    @SWG\Property(property="name", type="string"),
     *    @SWG\Property(property="email", type="string"),
     *    @SWG\Property(property="lastname", type="string"),
     *    @SWG\Property(property="middlename", type="string"),
     *    @SWG\Property(property="gender", type="string", enum={"male", "female"}),
     *    @SWG\Property(property="birthday", type="string"),
     *    @SWG\Property(property="about", type="string"),
     *    @SWG\Property(property="city", type="string"),
     *    @SWG\Property(property="avatar", type="file"),
     *    @SWG\Property(property="photo", type="file"),
     *    @SWG\Property(property="verification", type="array", @SWG\Items(type="file")),
     *    @SWG\Property(property="referral", type="string")
     * )
     *
     * @param MasterRequest $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function registerMaster(MasterRequest $request)
    {
        if (!($user = $this->user->create($request->validated(), UserType::TYPE_MASTER, $request->getReferral()))) {
            $this->response->error('Cannot register an executor master!', 202);
        }

        return $this->response->item($user, (new UserTransformer())->withExtraData())
            ->setStatusCode(201);
    }

    /**
     * @SWG\Post(
     *    path = "/api/register/service",
     *    tags={"User"},
     *    description="A service sign up",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/ServiceRequest"),
     *    ),
     *    @SWG\Response(
     *       response = 201,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/User")
     *    ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Cannot register an executor service!",
     *    ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     * ),
     *
     * @SWG\Definition(
     *    definition="ServiceRequest",
     *    type="object",
     *    @SWG\Property(property="phone", type="string"),
     *    @SWG\Property(property="email", type="string"),
     *    @SWG\Property(property="name", type="string"),
     *    @SWG\Property(property="about_service", type="string"),
     *    @SWG\Property(property="city", type="string"),
     *    @SWG\Property(property="street", type="string"),
     *    @SWG\Property(property="house", type="string"),
     *    @SWG\Property(property="building", type="string"),
     *    @SWG\Property(property="avatar", type="file"),
     *    @SWG\Property(property="service", type="array", @SWG\Items(type="file")),
     *    @SWG\Property(property="verification", type="array", @SWG\Items(type="file")),
     *    @SWG\Property(property="referral", type="string")
     * )
     *
     * @param ServiceRequest $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function registerService(ServiceRequest $request)
    {
        if (!($user = $this->user->create($request->validated(), UserType::TYPE_SERVICE, $request->getReferral()))) {
            $this->response->error('Cannot register an executor service!', 202);
        }

        return $this->response->item($user, (new UserTransformer())->withExtraData())
            ->setStatusCode(201);
    }
}