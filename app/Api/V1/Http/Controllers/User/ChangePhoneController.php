<?php
/**
 * Created by PhpStorm.
 * User: kastiel87
 * Date: 26.12.17
 * Time: 15:49
 */

namespace App\Api\V1\Http\Controllers\User;

use App\Api\V1\Http\Controllers\Controller;
use App\Repositories\Confirm\ConfirmRepository;
use Dingo\Api\Http\Response;
use App\Repositories\User\UserRepository;
use App\Api\V1\Http\Requests\User\ChangePhoneRequest;
use App\Api\V1\Http\Requests\User\ChangePhoneCodeRequest;

/**
 * Class ChangePhoneController
 * @package App\Api\V1\Http\Controllers\Auth
 */
class ChangePhoneController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * @var ConfirmRepository
     */
    protected $confirm;

    /**
     * ChangePhoneController constructor.
     *
     * @param UserRepository $user
     * @param ConfirmRepository $confirm
     */
    public function __construct(UserRepository $user, ConfirmRepository $confirm)
    {
        $this->middleware('api.auth');

        $this->user = $user;
        $this->confirm = $confirm;
    }

    /**
     * @SWG\Post(
     *    path = "/api/users/change-phone/send-code",
     *    tags={"User"},
     *    description="Use to send code while changing the phone",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/ChangePhoneCodeRequest"),
     *    ),
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *    ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Cannot send a confirm code!",
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * ),
     *
     * @SWG\Definition(
     *    definition="ChangePhoneCodeRequest",
     *    type="object",
     *    @SWG\Property(property="phone", type="string")
     * )
     *
     * @param ChangePhoneCodeRequest $request
     *
     * @return Response
     */
    public function sendCode(ChangePhoneCodeRequest $request)
    {
        if (!$this->confirm->create($this->auth->user(), $request->getPhone())) {
            $this->response->error('Cannot send a confirm code!', 202);
        }

        return $this->response()->noContent()
            ->setStatusCode(200);
    }
    /**
     * @SWG\Put(
     *    path = "/api/users/change-phone",
     *    tags={"User"},
     *    description="Confirmation of changing the phone",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/ChangePhoneRequest"),
     *    ),
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *     ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Cannot change a phone!",
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * ),
     *
     * @SWG\Definition(
     *    definition="ChangePhoneRequest",
     *    type="object",
     *    @SWG\Property(property="phone", type="string"),
     *    @SWG\Property(property="code", type="string")
     * )
     *
     * @param ChangePhoneRequest $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function change(ChangePhoneRequest $request)
    {
        if (!$this->confirm->isValid($user = $this->auth->user(), $request->getCode()) || !$this->user->update($user, $request->validated())) {
            $this->response->error('Cannot change a phone!', 202);
        }

        $this->confirm->invalidate($user, $request->getCode());

        return $this->response()->noContent()
            ->setStatusCode(200);
    }
}