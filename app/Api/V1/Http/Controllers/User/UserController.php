<?php
/**
 * Created by PhpStorm.
 * User: kastiel87
 * Date: 25.12.17
 * Time: 14:35
 */

namespace App\Api\V1\Http\Controllers\User;

use App\Models\Db\UserType;
use App\Api\V1\Http\Controllers\Controller;
use App\Transformers\UserTransformer;
use App\Repositories\User\UserRepository;
use App\Api\V1\Http\Requests\User\UpdateMasterRequest;
use App\Api\V1\Http\Requests\User\UpdateServiceRequest;
use App\Api\V1\Http\Requests\User\UpdateCustomerRequest;

/**
 * Class UserController
 *
 * @SWG\Definition(
 *    definition="User",
 *    type="object",
 *    @SWG\Property(property="id", type="integer"),
 *    @SWG\Property(property="active", type="boolean"),
 *    @SWG\Property(property="verified", type="boolean"),
 *    @SWG\Property(property="dispatch", type="boolean"),
 *    @SWG\Property(property="phone", type="string"),
 *    @SWG\Property(property="types", type="array",
 *       @SWG\Items(type="string")
 *    ),
 *    @SWG\Property(property="files", ref="#/definitions/Files"),
 *    @SWG\Property(property="profile", ref="#/definitions/Profile"),
 *    @SWG\Property(property="wallet", ref="#/definitions/Wallet")
 * ),
 *
 * @SWG\Definition(
 *    definition="Profile",
 *    type="object",
 *    @SWG\Property(property="email", type="string"),
 *    @SWG\Property(property="name", type="string"),
 *    @SWG\Property(property="lastname", type="string"),
 *    @SWG\Property(property="middlename", type="string"),
 *    @SWG\Property(property="gender", type="string"),
 *    @SWG\Property(property="birthday", type="string"),
 *    @SWG\Property(property="about", type="string"),
 *    @SWG\Property(property="city", type="string"),
 *    @SWG\Property(property="street", type="string"),
 *    @SWG\Property(property="house", type="string"),
 *    @SWG\Property(property="referral", type="string")
 * ),
 *
 * @SWG\Definition(
 *    definition="Wallet",
 *    type="object",
 *    @SWG\Property(property="balance", type="integer")
 * )
 *
 * @package App\Api\V1\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * UserController constructor.
     *
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->middleware('api.auth');

        $this->user = $user;
    }

    /**
     * @SWG\Get(
     *    path = "/api/users/profile",
     *    tags={"User"},
     *    description="An user profile info",
     *    produces={"application/json"},
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/User")
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * )
     *
     *
     * @return \Dingo\Api\Http\Response
     */
    public function get()
    {
        return $this->response->item(
            $this->auth->user(), (new UserTransformer())->withExtraData()
        );
    }

    /**
     * @SWG\Put(
     *    path = "/api/users/update/customer",
     *    tags={"User"},
     *    description="A customer update",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/UpdateCustomerRequest"),
     *    ),
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/User")
     *    ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Cannot update a customer!",
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    @SWG\Response(
     *       response = 403,
     *       description = "Forbidden",
     *    ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * ),
     *
     * @SWG\Definition(
     *    definition="UpdateCustomerRequest",
     *    type="object",
     *    @SWG\Property(property="name", type="string", description="Avoid to send when a user is verified and it is an executor also"),
     *    @SWG\Property(property="lastname", type="string", description="Avoid to send when a user is verified and it is an executor also"),
     *    @SWG\Property(property="middlename", type="string", description="Avoid to send when a user is verified and it is an executor also"),
     *    @SWG\Property(property="gender", type="string", enum={"male", "female"}, description="Avoid to send when a user is verified and it is an executor also"),
     *    @SWG\Property(property="birthday", type="string", description="Avoid to send when a user is verified and it is an executor also"),
     *    @SWG\Property(property="city", type="string"),
     *    @SWG\Property(property="street", type="string"),
     *    @SWG\Property(property="house", type="string"),
     *    @SWG\Property(property="avatar", type="file"),
     *    @SWG\Property(property="delete_files", type="array", @SWG\Items(type="integer"))
     * )
     *
     * @param UpdateCustomerRequest $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function updateCustomer(UpdateCustomerRequest $request)
    {
        $this->authorize('updateCustomer');

        if (!($user = $this->user->update($this->auth->user(), $request->validated(), $request->getDeletedFiles(), UserType::TYPE_COSTUMER))) {
            $this->response->error('Cannot update a customer!', 202);
        }

        return $this->response->item(
            $user, new UserTransformer()
        );
    }
    /**
     * @SWG\Put(
     *   path = "/api/users/update/master",
     *   tags={"User"},
     *   description="A master update",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *      name="body",
     *      in="body",
     *      required=true,
     *      @SWG\Schema(ref="#/definitions/UpdateMasterRequest"),
     *   ),
     *   @SWG\Response(
     *      response = 200,
     *      description = "Success",
     *      @SWG\Schema(ref="#/definitions/User")
     *   ),
     *   @SWG\Response(
     *      response = 202,
     *      description = "Cannot update a master!",
     *   ),
     *   @SWG\Response(
     *      response = 401,
     *      description = "Need authorization",
     *      @SWG\Schema(ref="#/definitions/Error")
     *   ),
     *   @SWG\Response(
     *      response = 403,
     *      description = "Forbidden",
     *   ),
     *   @SWG\Response(
     *      response = 422,
     *      description = "Validation errors",
     *      @SWG\Schema(ref="#/definitions/ValidationErrors")
     *   ),
     *   security={{
     *     "auth":{}
     *   }}
     * ),
     *
     * @SWG\Definition(
     *    definition="UpdateMasterRequest",
     *    type="object",
     *    @SWG\Property(property="name", type="string", description="Avoid to send when a user is verified"),
     *    @SWG\Property(property="lastname", type="string", description="Avoid to send when a user is verified"),
     *    @SWG\Property(property="middlename", type="string", description="Avoid to send when a user is verified"),
     *    @SWG\Property(property="gender", type="string", enum={"male", "female"}, description="Avoid to send when a user is verified"),
     *    @SWG\Property(property="birthday", type="string", description="Avoid to send when a user is verified"),
     *    @SWG\Property(property="about", type="string"),
     *    @SWG\Property(property="city", type="string"),
     *    @SWG\Property(property="avatar", type="file"),
     *    @SWG\Property(property="photo", type="file"),
     *    @SWG\Property(property="verification", type="array", @SWG\Items(type="file"), description="Avoid to send when a user is verified"),
     *    @SWG\Property(property="delete_files", type="array", @SWG\Items(type="integer"))
     * )
     *
     * @param UpdateMasterRequest $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function updateMaster(UpdateMasterRequest $request)
    {
        $this->authorize('updateMaster');

        if (!($user = $this->user->update($this->auth->user(), $request->validated(), $request->getDeletedFiles(), UserType::TYPE_MASTER))) {
            $this->response->error('Cannot update a master!', 202);
        }

        return $this->response->item(
            $user, (new UserTransformer())->withExtraData()
        );
    }

    /**
     * @SWG\Put(
     *    path = "/api/users/update/service",
     *    tags={"User"},
     *    description="A service update",
     *    produces={"application/json"},
     *    @SWG\Parameter(
     *       name="body",
     *       in="body",
     *       required=true,
     *       @SWG\Schema(ref="#/definitions/UpdateServiceRequest"),
     *    ),
     *    @SWG\Response(
     *       response = 200,
     *       description = "Success",
     *       @SWG\Schema(ref="#/definitions/User")
     *    ),
     *    @SWG\Response(
     *       response = 202,
     *       description = "Cannot update a service!",
     *    ),
     *    @SWG\Response(
     *       response = 401,
     *       description = "Need authorization",
     *       @SWG\Schema(ref="#/definitions/Error")
     *    ),
     *    @SWG\Response(
     *       response = 403,
     *       description = "Forbidden",
     *    ),
     *    @SWG\Response(
     *       response = 422,
     *       description = "Validation errors",
     *       @SWG\Schema(ref="#/definitions/ValidationErrors")
     *    ),
     *    security={{
     *      "auth":{}
     *    }}
     * ),
     *
     * @SWG\Definition(
     *    definition="UpdateServiceRequest",
     *    type="object",
     *    @SWG\Property(property="name", type="string", description="Avoid to send when a user is verified"),
     *    @SWG\Property(property="about", type="string"),
     *    @SWG\Property(property="city", type="string"),
     *    @SWG\Property(property="street", type="string"),
     *    @SWG\Property(property="house", type="string"),
     *    @SWG\Property(property="building", type="string"),
     *    @SWG\Property(property="avatar", type="file"),
     *    @SWG\Property(property="service", type="array", @SWG\Items(type="file")),
     *    @SWG\Property(property="verification", type="array", @SWG\Items(type="file"), description="Avoid to send when a user is verified"),
     *    @SWG\Property(property="delete_files", type="array", @SWG\Items(type="integer"))
     * )
     *
     * @param UpdateServiceRequest $request
     *
     * @return \Dingo\Api\Http\Response
     */
    public function updateService(UpdateServiceRequest $request)
    {
        $this->authorize('updateService');

        if (!($user = $this->user->update($this->auth->user(), $request->validated(), $request->getDeletedFiles(), UserType::TYPE_SERVICE))) {
            $this->response->error('Cannot update a service!', 202);
        }

        return $this->response->item(
            $user, (new UserTransformer())->withExtraData()
        );
    }
}