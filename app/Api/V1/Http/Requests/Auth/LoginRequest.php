<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 22.12.17
 * Time: 11:58
 */

namespace App\Api\V1\Http\Requests\Auth;

use App\Rules\PhoneRule;
use App\Api\V1\Http\Requests\Request;

/**
 * Class LoginRequest
 * @package App\Api\V1\Http\Requests\Auth
 */
class LoginRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'required', new PhoneRule(), 'exists:users,phone',
            ],
            'code' => 'required|string',
        ];
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->get('phone');
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->get('code');
    }
}