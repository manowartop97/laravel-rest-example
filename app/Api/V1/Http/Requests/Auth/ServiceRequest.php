<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 18.12.17
 * Time: 17:21
 */

namespace App\Api\V1\Http\Requests\Auth;

use App\Rules\PhoneRule;

/**
 * Class ServiceRequest
 * @package App\Api\V1\Http\Requests\Auth
 */
class ServiceRequest extends ExecutorRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'required', 'unique:users', new PhoneRule(),
            ],
            'email' => 'required|email|unique:user_profiles|max:70|min:6',
            'name'  => 'required|string|min:1|max:30',
            'city'      => 'required|string|max:255',
            'street'    => 'required|string|max:255',
            'house'     => 'required|string|max:255',
            'building'  => 'required|string|max:255',
            'about'     => 'nullable|string|min:1|max:500',
            'avatar'    => 'nullable|image|mimes:jpg,png,jpeg',
            'service.*' => 'nullable|image|mimes:jpg,png,jpeg',
            'verification.*' => 'nullable|image|mimes:jpg,png,jpeg',
        ] + parent::rules();
    }
}