<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 22.12.17
 * Time: 10:59
 */

namespace App\Api\V1\Http\Requests\Auth;

use App\Rules\PhoneRule;
use App\Api\V1\Http\Requests\Request;

/**
 * Class LoginCodeRequest
 * @package App\Api\V1\Http\Requests\Auth
 */
class LoginCodeRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'required', new PhoneRule(), 'exists:users,phone',
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->get('phone');
    }
}