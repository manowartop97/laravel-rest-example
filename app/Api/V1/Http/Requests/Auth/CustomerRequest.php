<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 18.12.17
 * Time: 15:01
 */

namespace App\Api\V1\Http\Requests\Auth;

use App\Rules\PhoneRule;
use App\Api\V1\Http\Requests\Request;

/**
 * Class CustomerRequest
 * @package App\Api\V1\Http\Requests\Auth
 */
class CustomerRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'required', 'unique:users', new PhoneRule(),
            ],
            'name' => 'required|string|min:1|max:30',
        ];
    }
}