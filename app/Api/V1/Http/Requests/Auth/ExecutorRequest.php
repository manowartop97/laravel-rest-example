<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 18.12.17
 * Time: 15:34
 */

namespace App\Api\V1\Http\Requests\Auth;

use App\Api\V1\Http\Requests\Request;

/**
 * Class ReferralRequest
 * @package App\Api\V1\Http\Requests\Auth
 */
class ExecutorRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'referral' => 'nullable|string|exists:user_profiles',
        ];
    }

    /**
     * @return array
     */
    public function validated()
    {
        return collect(parent::validated())->except(['referral'])->toArray();
    }

    /**
     * @return string|null
     */
    public function getReferral()
    {
        return $this->get('referral');
    }
}