<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 18.12.17
 * Time: 15:34
 */

namespace App\Api\V1\Http\Requests\Auth;

use App\Rules\PhoneRule;
use App\Models\Db\UserProfile;
use Illuminate\Validation\Rule;

/**
 * Class MasterRequest
 * @package App\Api\V1\Http\Requests\Auth
 */
class MasterRequest extends ExecutorRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'required', 'unique:users', new PhoneRule(),
            ],
            'email' => 'required|email|max:70|min:6|unique:user_profiles',
            'name'       => 'required|string|min:1|max:30',
            'lastname'   => 'required|string|min:1|max:30',
            'middlename' => 'required|string|min:1|max:30',
            'gender' => [
                'required', Rule::in([UserProfile::GENDER_MALE, UserProfile::GENDER_FEMALE]),
            ],
            'birthday' => 'required|date_format:Y-m-d',
            'city'     => 'required|string|max:255',
            'about'    => 'nullable|string|min:1|max:500',
            'avatar'   => 'nullable|image|mimes:jpg,png,jpeg',
            'photo'    => 'nullable|image|mimes:jpg,png,jpeg',
            'verification.*' => 'nullable|image|mimes:jpg,png,jpeg',
        ] + parent::rules();
    }
}