<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 12/29/17
 * Time: 1:46 PM
 */

namespace App\Api\V1\Http\Requests\Application;

use App\Api\V1\Http\Requests\Request;
use Illuminate\Database\Query\Builder;
use Illuminate\Validation\Rule;

/**
 * Class ApplicationCreateRequest
 *
 * @package App\Api\V1\Http\Requests
 */
class ApplicationCreateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_type_id' => [
                'required', Rule::exists('category_types', 'id')->where(function (Builder $query) {
                    $query->where('active', true);
                }),
            ],
            'brand_id' => [
                'required', Rule::exists('brands', 'id')->where(function (Builder $query) {
                    $query->where('active', true);
                }),
            ],
            'description' => 'required|string|min:1|max:3000',
            'photo.*'  => 'nullable|image|mimes:jpg,png,jpeg',
            'pickup'   => 'nullable:boolean',
            'city'     => 'required_without:pickup|string|max:255',
            'street'   => 'required_without:pickup|string|max:255',
            'house'    => 'required_without:pickup|string|max:255',
            'building' => 'required_without:pickup|string|max:255',
        ];
    }
}