<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 12/29/17
 * Time: 1:46 PM
 */

namespace App\Api\V1\Http\Requests\Application;

use App\Api\V1\Http\Requests\Request;
use App\Models\Db\Application;
use Illuminate\Validation\Rule;

/**
 * Class ApplicationStatusRequest
 *
 * @package App\Api\V1\Http\Requests
 */
class ApplicationStatusRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => [
                'required', Rule::in([Application::STATUS_COMPLETE, Application::STATUS_CANCEL]),
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return (int)$this->get('status', 0);
    }
}