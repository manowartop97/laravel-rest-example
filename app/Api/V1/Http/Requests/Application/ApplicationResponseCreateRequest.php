<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 12/29/17
 * Time: 1:46 PM
 */

namespace App\Api\V1\Http\Requests\Application;

use App\Api\V1\Http\Requests\Request;
use App\Models\Db\ApplicationResponse;
use Illuminate\Validation\Rule;

/**
 * Class ApplicationResponseCreateRequest
 *
 * @package App\Api\V1\Http\Requests
 */
class ApplicationResponseCreateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price' => 'required|integer',
            'price_type'  => [
                'required', Rule::in([ApplicationResponse::PRICE_TYPE_APPROX, ApplicationResponse::PRICE_TYPE_FIXED]),
            ],
            'description' => 'required|string|min:1|max:3000',
            'items'  => 'required|array',
            'items.*.name'  => 'required|string',
            'items.*.price' => 'required|integer',
        ];
    }

    /**
     * @return array
     */
    public function validated()
    {
        return collect(parent::validated())->except(['items'])->toArray();
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->get('items', []);
    }
}