<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 12/29/17
 * Time: 1:46 PM
 */

namespace App\Api\V1\Http\Requests\Application;

/**
 * Class ApplicationResponseCreateRequest
 *
 * @package App\Api\V1\Http\Requests
 */
class ApplicationResponseUpdateRequest extends ApplicationResponseCreateRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return parent::rules() + [
            'items.*.id' => 'required|integer',
        ];
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        $items = [];
        $parentItems = parent::getItems();

        foreach ($parentItems as $id => $item) {
            $items[$item['id']] = collect($item)->except(['id'])->toArray();
        }

        return $items;
    }
}