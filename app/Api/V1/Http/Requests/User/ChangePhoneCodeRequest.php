<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 26.12.17
 * Time: 17:02
 */

namespace App\Api\V1\Http\Requests\User;

use App\Api\V1\Http\Requests\Request;
use App\Rules\PhoneRule;

/**
 * Class ChangePhoneCodeRequest
 * @package App\Api\V1\Http\Requests\Auth
 */
class ChangePhoneCodeRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'required', new PhoneRule(), 'unique:users,phone'
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->get('phone');
    }
}