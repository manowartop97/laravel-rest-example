<?php
/**
 * Created by PhpStorm.
 * User: kastiel87
 * Date: 21.12.17
 * Time: 17:19
 */

namespace App\Api\V1\Http\Requests\User;

use Dingo\Api\Facade\API;

/**
 * Class UpdateServiceRequest
 * @package App\Api\V1\Http\Requests\Auth
 */
class UpdateServiceRequest extends UserRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->prepareRules([
            'name' => 'required|string|min:1|max:30',
            'city'      => 'required|max:255',
            'street'    => 'required|max:255',
            'house'     => 'required|max:255',
            'building'  => 'required|max:255',
            'about'     => 'nullable|string|min:1|max:500',
            'avatar'    => 'nullable|image|mimes:jpg,png,jpeg',
            'service.*' => 'nullable|image|mimes:jpg,png,jpeg',
            'verification.*' => 'nullable|image|mimes:jpg,png,jpeg',
        ] + parent::rules(), API::user());
    }
}