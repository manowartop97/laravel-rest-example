<?php
/**
 * Created by PhpStorm.
 * User: kastiel87
 * Date: 21.12.17
 * Time: 16:01
 */

namespace App\Api\V1\Http\Requests\User;

use App\Models\Db\UserProfile;
use Dingo\Api\Facade\API;
use Illuminate\Validation\Rule;

/**
 * Class UpdateMasterRequest
 *
 * @package App\Api\V1\Http\Requests\Auth
 */
class UpdateMasterRequest extends UserRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->prepareRules([
            'name'       => 'required|string|min:1|max:30',
            'lastname'   => 'required|string|min:1|max:30',
            'middlename' => 'required|string|min:1|max:30',
            'gender' => [
                'required', Rule::in([UserProfile::GENDER_MALE, UserProfile::GENDER_FEMALE]),
            ],
            'birthday' => 'required|date_format:Y-m-d',
            'city'     => 'required|max:255',
            'about'  => 'nullable|string|min:1|max:500',
            'avatar' => 'nullable|image|mimes:jpg,png,jpeg',
            'photo'  => 'nullable|image|mimes:jpg,png,jpeg',
            'verification.*' => 'nullable|image|mimes:jpg,png,jpeg',
        ] + parent::rules(), API::user());
    }
}