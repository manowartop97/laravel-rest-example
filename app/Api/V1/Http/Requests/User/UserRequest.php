<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 21.12.17
 * Time: 16:01
 */

namespace App\Api\V1\Http\Requests\User;

use App\Models\Db\User;
use Illuminate\Database\Eloquent\Model;
use App\Api\V1\Http\Requests\RequestFiles;

/**
 * Class UpdateMasterRequest
 *
 * @package App\Api\V1\Http\Requests\Auth
 */
class UserRequest extends RequestFiles
{
    /**
     * @var array
     */
    protected static $exceptFields = [
        'name',
        'gender',
        'birthday',
        'lastname',
        'middlename',
        'verification.*',
    ];

    /**
     * @param array $rules
     * @param User|Model $user
     * @return array
     */
    protected function prepareRules(array $rules, User $user)
    {
        if ($user->verified && $user->isExecutor()) {
            return collect($rules)->except(self::$exceptFields)->toArray();
        }

        return $rules;
    }
}