<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 25.12.17
 * Time: 17:50
 */

namespace App\Api\V1\Http\Requests\User;

use App\Models\Db\UserProfile;
use Dingo\Api\Facade\API;
use Illuminate\Validation\Rule;

/**
 * Class UpdateCustomerRequest
 * @package App\Api\V1\Http\Requests\Auth
 */
class UpdateCustomerRequest extends UserRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->prepareRules([
            'name'       => 'required|string|min:1|max:30',
            'lastname'   => 'required|string|min:1|max:30',
            'middlename' => 'required|string|min:1|max:30',
            'gender' => [
                'nullable', Rule::in([UserProfile::GENDER_MALE, UserProfile::GENDER_FEMALE]),
            ],
            'birthday' => 'nullable|date_format:Y-m-d',
            'city'     => 'nullable|string|max:255',
            'street'   => 'nullable|string|max:255',
            'house'    => 'nullable|string|max:255',
            'avatar'   => 'nullable|image|mimes:jpg,png,jpeg',
        ] + parent::rules(), API::user());
    }
}