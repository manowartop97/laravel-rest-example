<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 27.12.17
 * Time: 10:04
 */

namespace App\Api\V1\Http\Requests\User;

use App\Api\V1\Http\Requests\Request;
use App\Rules\PhoneRule;

/**
 * Class ChangePhoneRequest
 * @package App\Api\V1\Http\Requests\Auth
 */
class ChangePhoneRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'required', new PhoneRule()
            ],
            'code' => 'required|numeric',
        ];
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->get('code');
    }

    /**
     * @return array
     */
    public function validated()
    {
        return collect(parent::validated())->except(['code'])->toArray();
    }
}