<?php

namespace App\Api\V1\Http\Requests;

use Dingo\Api\Http\FormRequest;

/**
 * Class Request
 * @package App\Http\Requests
 */
class Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}