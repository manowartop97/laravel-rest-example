<?php

namespace App\Api\V1\Http\Requests;

/**
 * Class Request
 * @package App\Http\Requests
 */
class RequestFiles extends Request
{
    /**
     *
     */
    const FIELD_NAME = 'delete_files';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::FIELD_NAME => 'nullable|array',
            self::FIELD_NAME . '.*' => 'nullable|integer',
        ];
    }

    /**
     * @return array
     */
    public function validated()
    {
        return collect(parent::validated())->except([self::FIELD_NAME])->toArray();
    }

    /**
     * @return mixed
     */
    public function getDeletedFiles()
    {
        return collect($this->get(self::FIELD_NAME, []))->map(function ($val) {
            return (int)$val;
        })->toArray();
    }
}