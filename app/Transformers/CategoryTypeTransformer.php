<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 19.12.17
 * Time: 17:42
 */

namespace App\Transformers;

use App\Models\Db\CategoryType;
use League\Fractal\TransformerAbstract;

/**
 * Class CategoryTypeTransformer
 * @package App\Transformers
 */
class CategoryTypeTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'brands',
    ];

    /**
     * @param CategoryType $type
     *
     * @return array
     */
    public function transform(CategoryType $type)
    {
        return [
            'id'   => $type->id,
            'name' => $type->name,
        ];
    }

    /**
     * @param CategoryType $type
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeBrands(CategoryType $type)
    {
        return $this->collection($type->brands, new BrandTransformer(), false);
    }
}