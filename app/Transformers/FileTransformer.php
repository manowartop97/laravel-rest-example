<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 19.12.17
 * Time: 17:42
 */

namespace App\Transformers;

use App\Models\Db\File;
use Illuminate\Support\Facades\Storage;
use League\Fractal\TransformerAbstract;

/**
 * Class FileTransformer
 * @package App\Transformers
 */
class FileTransformer extends TransformerAbstract
{
    /**
     * @var string
     */
    protected $disk;

    /**
     * @var array
     */
    protected $excludeTypes;

    /**
     * ApplicationTransformer constructor.
     *
     * @param string $disk
     * @param array $excludeTypes
     */
    public function __construct($disk, array $excludeTypes = [])
    {
        $this->disk = $disk;
        $this->excludeTypes = $excludeTypes;
    }

    /**
     * @param File[] $files
     *
     * @return array
     */
    public function transform($files)
    {
        $data = [];

        foreach ($files as $file) {

            if (\in_array($file->type, $this->excludeTypes, true)) {
                continue;
            }

            $data[] = [
                'id'   => $file->id,
                'type' => $file->type,
                'url'  => Storage::disk($this->disk)->url($file->name),
            ];
        }

        return $data;
    }
}