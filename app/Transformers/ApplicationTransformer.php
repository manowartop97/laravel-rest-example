<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 19.12.17
 * Time: 17:42
 */

namespace App\Transformers;

use App\Models\Db\Application;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\TransformerAbstract;

/**
 * Class ApplicationTransformer
 * @package App\Transformers
 */
class ApplicationTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'user',
    ];

    /**
     * @param Application $application
     *
     * @return array
     */
    public function transform(Application $application)
    {
        return [
            'id'             => $application->id,
            'category'       => $application->categoryType->name,
            'brand'          => $application->brand->name,
            'status'         => $application->status,
            'description'    => $application->description,
            'city'           => $application->city,
            'street'         => $application->street,
            'house'          => $application->house,
            'building'       => $application->building,
            'pickup'         => (boolean)$application->pickup,
            'viewsCount'     => $application->viewsCount,
            'responsesCount' => $application->responsesCount,
        ];
    }

    /**
     * @param Application $application
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(Application $application)
    {
        return $this->item($application->user, new UserTransformer());
    }

    /**
     * @param Application $application
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeFiles(Application $application)
    {
        return $this->item($application->files, new FileTransformer(Application::DISK));
    }

    /**
     * @param Application $application
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeResponses(Application $application)
    {
        return $this->collection($paginator = $application->responses()->with('user.files')->paginate(), new ApplicationResponseTransformer())
            ->setPaginator(new IlluminatePaginatorAdapter($paginator));
    }

    /**
     * @return static
     */
    public function withIncludes()
    {
        $this->defaultIncludes = array_merge($this->defaultIncludes, [
            'responses',
            'files',
        ]);

        return $this;
    }
}