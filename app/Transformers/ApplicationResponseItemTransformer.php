<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 19.12.17
 * Time: 17:42
 */

namespace App\Transformers;

use App\Models\Db\ApplicationResponseItem;
use League\Fractal\TransformerAbstract;

/**
 * Class ApplicationResponseTransformer
 *
 * @package App\Transformers
 */
class ApplicationResponseItemTransformer extends TransformerAbstract
{
    /**
     * @param ApplicationResponseItem $item
     * @return array
     */
    public function transform(ApplicationResponseItem $item)
    {
        return [
            'id'    => $item->id,
            'name'  => $item->name,
            'price' => $item->price,
        ];
    }
}