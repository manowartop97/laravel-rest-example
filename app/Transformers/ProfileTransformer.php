<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 19.12.17
 * Time: 17:42
 */

namespace App\Transformers;

use App\Models\Db\UserProfile;
use League\Fractal\TransformerAbstract;

/**
 * Class ProfileTransformer
 *
 * @package App\Transformers
 */
class ProfileTransformer extends TransformerAbstract
{
    /**
     * @param UserProfile $profile
     *
     * @return array
     */
    public function transform(UserProfile $profile)
    {
        return [
            'email'      => $profile->email,
            'name'       => $profile->name,
            'lastname'   => $profile->lastname,
            'middlename' => $profile->middlename,
            'gender'     => $profile->gender,
            'birthday'   => ($birthday = $profile->birthday) ? $birthday->toDateString() : null,
            'about'      => $profile->about,
            'city'       => $profile->city,
            'street'     => $profile->street,
            'house'      => $profile->house,
            'referral'   => $profile->referral,
        ];
    }
}