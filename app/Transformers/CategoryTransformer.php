<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 19.12.17
 * Time: 17:42
 */

namespace App\Transformers;

use App\Models\Db\Category;
use League\Fractal\TransformerAbstract;

/**
 * Class CategoryTypeTransformer
 * @package App\Transformers
 */
class CategoryTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'types',
    ];

    /**
     * @param Category $category
     *
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'id'   => $category->id,
            'name' => $category->name,
        ];
    }

    /**
     * @param Category $category
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTypes(Category $category)
    {
        return $this->collection($category->types, new CategoryTypeTransformer(), false);
    }
}