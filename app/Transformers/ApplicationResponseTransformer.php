<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 19.12.17
 * Time: 17:42
 */

namespace App\Transformers;

use App\Models\Db\ApplicationResponse;
use League\Fractal\TransformerAbstract;

/**
 * Class ApplicationResponseTransformer
 *
 * @package App\Transformers
 */
class ApplicationResponseTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'user',
        'items',
    ];

    /**
     * @param ApplicationResponse $response
     *
     * @return array
     */
    public function transform(ApplicationResponse $response)
    {
        return [
            'id'          => $response->id,
            'price'       => $response->price,
            'price_type'  => $response->price_type,
            'selected'    => $response->selected,
            'description' => $response->description,
        ];
    }

    /**
     * @param ApplicationResponse $response
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeItems(ApplicationResponse $response)
    {
        return $this->collection($response->items, new ApplicationResponseItemTransformer(), false);
    }

    /**
     * @param ApplicationResponse $response
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(ApplicationResponse $response)
    {
        return $this->item($response->user, new UserTransformer());
    }
}