<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 19.12.17
 * Time: 17:42
 */

namespace App\Transformers;

use App\Models\Db\UserWallet;
use League\Fractal\TransformerAbstract;

/**
 * Class WalletTransformer
 *
 * @package App\Transformers
 */
class WalletTransformer extends TransformerAbstract
{
    /**
     * @param UserWallet $wallet
     *
     * @return array
     */
    public function transform(UserWallet $wallet)
    {
        return [
            'balance' => $wallet->balance,
        ];
    }
}