<?php
/**
 * Created by PhpStorm.
 * User: kastiel87
 * Date: 19.12.17
 * Time: 17:42
 */

namespace App\Transformers;

use App\Models\Db\User;
use Illuminate\Support\Arr;
use League\Fractal\TransformerAbstract;

/**
 * Class UserTransformer
 * @package App\Transformers
 */
class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'profile',
        'files',
    ];

    /**
     * @var array
     */
    protected $excludeFilesTypes = [
        'verification',
    ];

    /**
     * @param User $user
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id'       => $user->id,
            'active'   => (bool)$user->active,
            'verified' => (bool)$user->verified,
            'dispatch' => (bool)$user->dispatch,
            'phone'    => $user->phone,
            'types'    => $user->userTypes->pluck('type'),
        ];
    }

    /**
     * @param User $user
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeProfile(User $user)
    {
        return $this->item($user->profile, new ProfileTransformer());
    }

    /**
     * @param User $user
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeFiles(User $user)
    {
        return $this->item($user->files, new FileTransformer(User::DISK, $this->excludeFilesTypes));
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Item|\League\Fractal\Resource\NullResource
     */
    public function includeWallet(User $user)
    {
        return $user->wallet ? $this->item($user->wallet, new WalletTransformer()) : $this->null();
    }

    /**
     * @return static
     */
    public function withExtraData()
    {
        Arr::forget($this->excludeFilesTypes, 'verification');

        $this->defaultIncludes[] = 'wallet';

        return $this;
    }
}