<?php
/**
 * Created by PhpStorm.
 * User: andrusik
 * Date: 19.12.17
 * Time: 17:42
 */

namespace App\Transformers;

use App\Models\Db\Brand;
use League\Fractal\TransformerAbstract;

/**
 * Class UserTransformer
 * @package App\Transformers
 */
class BrandTransformer extends TransformerAbstract
{
    /**
     * @param Brand $brand
     *
     * @return array
     */
    public function transform(Brand $brand)
    {
        return [
            'id'   => $brand->id,
            'name' => $brand->name,
        ];
    }
}