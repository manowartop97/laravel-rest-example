<?php

namespace App\Console\Commands;

use App\Repositories\Application\ApplicationRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;

/**
 * Class ApplicationCommand
 *
 * @package App\Console\Commands
 */
class ApplicationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application:cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel not confirmed applications';

    /**
     * @var ApplicationRepository
     */
    protected $application;

    /**
     * ApplicationCommand constructor.
     *
     * @param ApplicationRepository $application
     */
    public function __construct(ApplicationRepository $application)
    {
        parent::__construct();
        $this->application = $application;
    }

    /**
     *
     */
    public function handle()
    {
        $processed = $this->application->processWithoutResponses(
            Carbon::now()->addDay(-2)
        );

        if ($processed) {

            $processed = $this->application->processNotFinished(
                Carbon::now()->addDay(-7)
            );
        }

        if ($processed) {
            $this->info('Applications processed !');
        } else {
            $this->error('Applications not processed !');
        }
    }
}
