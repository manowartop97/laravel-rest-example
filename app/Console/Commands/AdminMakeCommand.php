<?php

namespace App\Console\Commands;

use Encore\Admin\Console\MakeCommand;

/**
 * Class AdminMakeCommand
 *
 * @package App\Console\Commands
 */
class AdminMakeCommand extends MakeCommand
{
    /**
     * @param string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return config('admin.route.namespace');
    }
}
