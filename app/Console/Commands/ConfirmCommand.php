<?php

namespace App\Console\Commands;

use App\Repositories\Confirm\ConfirmRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;

/**
 * Class ConfirmCommand
 *
 * @package App\Console\Commands
 */
class ConfirmCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'confirm:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear old confirms';

    /**
     * @var ConfirmRepository
     */
    protected $confirm;

    /**
     * ApplicationCommand constructor.
     *
     * @param ConfirmRepository $confirm
     */
    public function __construct(ConfirmRepository $confirm)
    {
        parent::__construct();
        $this->confirm = $confirm;
    }

    /**
     *
     */
    public function handle()
    {
        $processed = $this->confirm->processNotUsed(
            Carbon::now()
        );

        if ($processed) {
            $this->info('Confirms processed !');
        } else {
            $this->error('Confirms not processed !');
        }
    }
}
