<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class PaymentHttpException
 *
 * @package App\Exceptions
 */
class PaymentHttpException extends HttpException
{
    /**
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     */
    public function __construct($message = 'Not enough money', \Exception $previous = null, $code = 0)
    {
        parent::__construct(402, $message, $previous, [], $code);
    }
}
