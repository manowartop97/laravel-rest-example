<?php

if (!function_exists('unique_random')) {
    /**
     * @param string $table
     * @param string $column
     * @param int $length
     * @return string
     */
    function unique_random($table, $column, $length = 7)
    {

        do {

            $code = str_random($length);

            $exists = DB::table($table)
                ->where($column, '=', $code)
                ->exists();

        } while ($exists);

        return $code;
    }
}