<?php

namespace App\Listeners;

use App\Facades\Sms;
use App\Events\SendConfirmSmsEvent;

/**
 * Class SendConfirmSmsListener
 * @package App\Listeners
 */
class SendConfirmSmsListener
{
    /**
     * @param SendConfirmSmsEvent $event
     */
    public function handle($event)
    {
        Sms::send($event->phone, $event->code);
    }
}