<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class ProdDeny
 *
 * @package App\Http\Middleware
 */
class DenyProd
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        abort_if(app()->environment() === 'production', 403);

        return $next($request);
    }
}
