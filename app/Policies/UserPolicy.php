<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 1/2/18
 * Time: 3:46 PM
 */

namespace App\Policies;

use App\Models\Db\User;

/**
 * Class UserPolicy
 *
 * @package App\Policies
 */
class UserPolicy
{
    /**
     * @param User $user
     *
     * @return bool
     */
    public function updateCustomer(User $user)
    {
        return $user->isCustomer();
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function updateMaster(User $user)
    {
        return !$user->isService();
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function updateService(User $user)
    {
        return !$user->isMaster();
    }
}