<?php

namespace App\Policies;

use App\Exceptions\PaymentHttpException;
use App\Models\Db\Application;
use App\Models\Db\ApplicationResponse;
use App\Models\Db\User;
use App\Repositories\Setting\SettingRepository;

/**
 * Class ApplicationResponsePolicy
 *
 * @package App\Policies
 */
class ApplicationResponsePolicy
{
    /**
     * @var SettingRepository
     */
    protected $setting;

    /**
     * ApplicationResponsePolicy constructor.
     *
     * @param SettingRepository $setting
     */
    public function __construct(SettingRepository $setting)
    {
        $this->setting = $setting;
    }

    /**
     * @param User $user
     * @param Application $application
     *
     * @return bool
     */
    public function create(User $user, Application $application)
    {
        return $user->verified && $this->isNew($application) && !$this->isOwnerApplication($user, $application) &&
            $user->isExecutor() && $this->isNotAlreadyExists($user, $application) && $this->isEnoughMoney($user);
    }

    /**
     * @param User $user
     * @param ApplicationResponse $response
     *
     * @return bool
     */
    public function update(User $user, ApplicationResponse $response)
    {
        return $user->verified && $this->isNew($response->application) && $this->isOwner($user, $response);
    }

    /**
     * @param User $user
     * @param ApplicationResponse $response
     *
     * @return bool
     */
    public function confirm(User $user, ApplicationResponse $response)
    {
        return $user->verified && $this->isNew($response->application) && $this->isOwnerApplication($user, $response->application);
    }

    /**
     * @param Application $application
     *
     * @return bool
     */
    protected function isNew(Application $application)
    {
        return $application->status === Application::STATUS_NEW;
    }

    /**
     * @param User $user
     * @param ApplicationResponse $response
     *
     * @return bool
     */
    protected function isOwner(User $user, ApplicationResponse $response)
    {
        return $user->id === $response->user_id;
    }

    /**
     * @param User $user
     * @param Application $application
     *
     * @return bool
     */
    protected function isOwnerApplication(User $user, Application $application)
    {
        return $user->id === $application->user_id;
    }

    /**
     * @param User $user
     * @param Application $application
     *
     * @return bool
     */
    protected function isNotAlreadyExists(User $user, Application $application)
    {
        return ApplicationResponse::whereApplicationId($application->id)
            ->whereUserId($user->id)->exists() ? false : true;
    }

    /**
     * @param User $user
     * @return bool
     */
    protected function isEnoughMoney(User $user)
    {
        if ($user->wallet->balance < $this->setting->applicationResponsePrice()) {
            throw new PaymentHttpException();
        }

        return true;
    }
}
