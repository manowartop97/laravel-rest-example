<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 1/2/18
 * Time: 3:46 PM
 */

namespace App\Policies;

use App\Models\Db\Application;
use App\Models\Db\User;

/**
 * Class ApplicationPolicy
 *
 * @package App\Policies
 */
class ApplicationPolicy
{
    /**
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user)
    {
        return $user->isCustomer();
    }

    /**
     * @param User $user
     * @param Application $application
     *
     * @return bool
     */
    public function update(User $user, Application $application)
    {
        return $this->isOwner($user, $application) && $this->isNew($application);
    }

    /**
     * @param User $user
     * @param Application $application
     *
     * @return bool
     */
    public function show(User $user, Application $application)
    {
        return $this->isOwner($user, $application) || ($user->isExecutor() && ($this->isNew($application) || $this->isExecutor($user, $application)));
    }

    /**
     * @param User $user
     * @param Application $application
     * @param integer $status
     *
     * @return bool
     */
    public function changeStatus(User $user, Application $application, $status)
    {
        return $this->isOwner($user, $application) && $this->isValidStatus($application, $status);
    }

    /**
     * @param Application $application
     *
     * @return bool
     */
    protected function isNew(Application $application)
    {
        return $application->status === Application::STATUS_NEW;
    }

    /**
     * @param User $user
     * @param Application $application
     *
     * @return bool
     */
    protected function isExecutor(User $user, Application $application)
    {
        return (($executor = $application->executor) && ($user->id === $executor->id));
    }

    /**
     * @param User $user
     * @param Application $application
     *
     * @return bool
     */
    protected function isOwner(User $user, Application $application)
    {
        return $user->id === $application->user_id;
    }

    /**
     * @param Application $application
     * @param integer $status
     *
     * @return bool
     */
    protected function isValidStatus(Application $application, $status)
    {
        return $application->status !== Application::STATUS_COMPLETE && $application->status < $status;
    }
}