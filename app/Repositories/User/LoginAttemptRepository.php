<?php

namespace App\Repositories\User;

use Carbon\Carbon;
use App\Models\Db\User;
use App\Models\Db\UserLoginAttempt;

/**
 * Class LoginAttemptRepository
 * @package App\Repositories\UserLoginAttempt
 */
class LoginAttemptRepository
{
    /**
     * @param User|mixed $user
     *
     * @return User|bool
     */
    public function check(User $user)
    {
        $attempt = UserLoginAttempt::find($user->id);

        if (!$attempt) {

            $attempt = new UserLoginAttempt(['expiration' => Carbon::now()->addDay()]);
            $attempt->user()->associate($user);
        }

        $attempt->count++;

        if (Carbon::now() >= $attempt->expiration) {

            $attempt->count = 1;
            $attempt->expiration = Carbon::now()->addDay();
        }

        return $attempt->save() && $attempt->count <= UserLoginAttempt::COUNT_ATTEMPTS;
    }
}