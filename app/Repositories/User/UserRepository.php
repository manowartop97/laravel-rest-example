<?php

namespace App\Repositories\User;

use App\Models\Db\User;
use App\Models\Db\UserType;
use App\Repositories\File\FileRepository;
use App\Events\MasterRegisteredEvent;
use App\Events\ServiceRegisteredEvent;
use App\Events\CustomerRegisteredEvent;
use App\Repositories\Referral\ReferralRepository;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Confirm\ConfirmRepository;

/**
 * Class UserRepository
 *
 * @package App\Repositories\User
 */
class UserRepository
{
    /**
     * @var ConfirmRepository
     */
    protected $confirm;

    /**
     * @var FileRepository
     */
    protected $file;

    /**
     * @var ReferralRepository
     */
    protected $referral;

    /**
     * @var UserWalletRepository
     */
    protected $wallet;

    /**
     * UserRepository constructor.
     *
     * @param ConfirmRepository $confirm
     * @param FileRepository $file
     * @param ReferralRepository $referral
     * @param UserWalletRepository $wallet
     */
    public function __construct(ConfirmRepository $confirm, FileRepository $file, ReferralRepository $referral, UserWalletRepository $wallet)
    {
        $this->file = $file;
        $this->file->init(User::DISK);

        $this->wallet = $wallet;
        $this->confirm = $confirm;
        $this->referral = $referral;
    }

    /**
     * @param array $data
     * @param string $type
     * @param string|null $referral
     *
     * @return false|Model|User
     */
    public function create(array $data, $type, $referral = null)
    {
        \DB::beginTransaction();

        try {

            $files = $this->file->prepare($data);

            $user = User::create($data);

            $user->userTypes()->create([
                'type' => $type
            ]);

            if ($type !== UserType::TYPE_COSTUMER) {

                $this->wallet->create($user);
                $this->referral->process($user, $referral);
            }

            $user->profile()->create($data);

            if ($this->file->store($user, $files) && $this->confirm->create($user)) {

                \DB::commit();

                switch ($type) {

                    case UserType::TYPE_COSTUMER:
                        event(new CustomerRegisteredEvent($user));
                    break;

                    case UserType::TYPE_MASTER:
                        event(new MasterRegisteredEvent($user));
                    break;

                    case UserType::TYPE_SERVICE:
                        event(new ServiceRegisteredEvent($user));
                    break;
                }

                return $user;
            }

        } catch(\Exception $e) {
            report($e);
        }

        \DB::rollBack();

        return false;
    }

    /**
     * @param User|mixed $user
     * @param array $data
     * @param array $deletedFiles
     * @param string|null $type a user type
     *
     * @return false|Model|User
     */
    public function update(User $user, array $data, array $deletedFiles = [], $type = null)
    {
        \DB::beginTransaction();

        try {

            $files = $this->file->prepare($data);

            if ($type || !$user->isUserType([$type])) {

                $this->wallet->update($user);

                $user->userTypes()->create([
                    'type' => $type,
                ]);
            }

            $user->profile()->update($data);

            if ($this->file->store($user, $files, $deletedFiles)) {

                \DB::commit();

                return $user;
            }

        } catch(\Exception $e) {
            report($e);
        }

        \DB::rollBack();

        return false;
    }

    /**
     * @param string $phone
     *
     * @return Model|null|User
     */
    public function getByPhone($phone)
    {
        return User::whereActive(true)
            ->wherePhone($phone)
            ->first();
    }
}