<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 1/16/18
 * Time: 10:09 AM
 */

namespace App\Repositories\User;

use App\Models\Db\User;
use App\Models\Db\UserWallet;
use App\Models\Db\UserWalletTransaction;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserWalletRepository
 *
 * @package App\Repositories\User
 */
class UserWalletRepository
{
    /**
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user)
    {
        return (new UserWallet())->user()->associate($user)->save();
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function update(User $user)
    {
        if ($user->wallet) {
            return true;
        }

        return $this->create($user);
    }

    /**
     * @param User $user
     * @param integer $value
     * @param Model $object morph object
     *
     * @return bool
     */
    public function payForReferral(User $user, $value, Model $object = null)
    {
        if ($value) {

            if ($wallet = $this->changeBalance($user, $value)) {
                return $this->addTransaction($wallet, $value, $object);
            }

            return false;
        }

        return true;
    }

    /**
     * @param User $user
     * @param integer $value
     * @param Model $object morph object
     *
     * @return bool
     */
    public function payForAppResponse(User $user, $value, Model $object)
    {
        if ($value) {

            if ($wallet = $this->changeBalance($user, -$value)) {
                return $this->addTransaction($wallet, $value, $object);
            }

            return false;
        }

        return true;
    }

    /**
     * @param User $user
     * @param integer $value
     *
     * @return UserWallet|false
     */
    protected function changeBalance(User $user, $value)
    {
        if ($user->wallet) {

            $user->wallet->balance += $value;

            if ($user->wallet->save()) {
                return $user->wallet;
            }
        }

        return false;
    }

    /**
     * @param UserWallet $wallet
     * @param string $value
     * @param Model $object morph object
     *
     * @return bool
     */
    protected function addTransaction(UserWallet $wallet, $value, Model $object = null)
    {
        $transaction = new UserWalletTransaction([
            'value' => $value,
        ]);

        $transaction->wallet()->associate($wallet);

        if ($object) {
            $transaction->object()->associate($object);
        }

        return $transaction->save();
    }
}