<?php

namespace App\Repositories\Confirm;

use App\Events\SendConfirmSmsEvent;
use Carbon\Carbon;
use App\Models\Db\User;
use App\Models\Db\Confirm;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ConfirmRepository
 * @package App\Repositories\Confirm
 */
class ConfirmRepository
{
    /**
     * @param User|Model $user
     * @param string|null $phone
     *
     * @return bool
     */
    public function create(User $user, $phone = null)
    {
        /**@var Confirm $confirm*/
        $confirm = $user->confirms()->create([
            'expiration' => Carbon::now()->addMinutes(15)
        ]);

        if ($confirm) {

            event(new SendConfirmSmsEvent($phone ?: $user->phone, $confirm->code));

            return true;
        }

        return false;
    }

    /**
     * @param User|mixed $user
     * @param string $code
     *
     * @return boolean
     */
    public function isValid(User $user, $code)
    {
        return ($confirm = $this->find($user, $code)) && $confirm->expiration >= Carbon::now();
    }

    /**
     * @param User|mixed $user
     * @param string $code
     *
     * @return bool
     */
    public function invalidate(User $user, $code)
    {
        if ($confirm = $this->find($user, $code)) {

            $confirm->expiration = Carbon::now();

            return $confirm->save();
        }

        return true;
    }

    /**
     * @param Carbon $date
     *
     * @return bool
     */
    public function processNotUsed(Carbon $date)
    {
        /**@var $query Builder*/
        $query = Confirm::whereDate('expiration', '<', $date);

        /**@var $confirm Confirm*/
        foreach ($query->cursor() as $confirm) {

            if (!$confirm->delete()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param User|mixed $user
     * @param string $code
     *
     * @return Confirm|null|Model
     */
    protected function find(User $user, $code)
    {
        /**@var Confirm $confirm*/
        return $user->confirms()->where('code', $code)
            ->first();
    }
}