<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 12/29/17
 * Time: 2:08 PM
 */

namespace App\Repositories\Application;

use App\Events\ApplicationResponseCreatedEvent;
use App\Models\Db\Application;
use App\Models\Db\ApplicationResponse;
use App\Models\Db\User;
use App\Repositories\User\UserWalletRepository;
use App\Repositories\Setting\SettingRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ApplicationResponseRepository.php
 *
 * @package App\Repositories\Application
 */
class ApplicationResponseRepository
{
    /**
     * @var SettingRepository
     */
    protected $setting;

    /**
     * @var UserWalletRepository
     */
    protected $wallet;

    /**
     * ApplicationResponseRepository constructor.
     *
     * @param SettingRepository $setting
     * @param UserWalletRepository $wallet
     */
    public function __construct(SettingRepository $setting, UserWalletRepository $wallet)
    {
        $this->wallet = $wallet;
        $this->setting = $setting;
    }

    /**
     * @param User|mixed $user
     * @param Application $application
     * @param array $data
     * @param array $items
     *
     * @return false|Model|ApplicationResponse
     */
    public function create(User $user, Application $application, array $data, array $items)
    {
        \DB::beginTransaction();

        try {

            $response = new ApplicationResponse($data);
            $response->user()->associate($user);
            $response->application()->associate($application);

            if ($response->save() && $this->wallet->payForAppResponse($user, $this->setting->applicationResponsePrice(), $response)) {

                $response->items()->createMany($items);

                event(new ApplicationResponseCreatedEvent($response));

                \DB::commit();

                return $response;
            }

        } catch(\Exception $e) {
            report($e);
        }

        \DB::rollBack();

        return false;
    }

    /**
     * @param ApplicationResponse $response
     * @param array $data
     * @param array $items
     *
     * @return false|Model|ApplicationResponse
     */
    public function update(ApplicationResponse $response, array $data, array $items = [])
    {
        \DB::beginTransaction();

        try {

            if ($response->update($data)) {

                foreach ($items as $id => $item) {

                    $response->items()->where('id', $id)
                        ->update($item);
                }

                \DB::commit();

                return $response;
            }

        } catch (\Exception $e) {
            report($e);
        }

        \DB::rollBack();

        return false;
    }

    /**
     * @param ApplicationResponse $response
     *
     * @return false|Model|ApplicationResponse
     */
    public function confirm(ApplicationResponse $response)
    {
        \DB::beginTransaction();

        try {

            $response->application->status = Application::STATUS_PROCESS;
            $response->selected = true;

            if ($response->save() && $response->application->save()) {
                return $response;
            }

        } catch (\Exception $e) {}

        \DB::rollBack();

        return false;
    }

    /**
     * @param integer $id
     *
     * @return null|ApplicationResponse|Model
     */
    public function getById($id)
    {
        return ApplicationResponse::find($id);
    }
}