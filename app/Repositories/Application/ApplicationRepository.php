<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 12/29/17
 * Time: 2:08 PM
 */

namespace App\Repositories\Application;

use App\Events\ApplicationCreatedEvent;
use App\Models\Db\Application;
use App\Models\Db\ApplicationView;
use App\Models\Db\User;
use App\Repositories\File\FileRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ApplicationRepository
 *
 * @package App\Repositories\Application
 */
class ApplicationRepository
{
    /**
     * @var FileRepository
     */
    protected $file;

    /**
     * ApplicationRepository constructor.
     *
     * @param FileRepository $file
     */
    public function __construct(FileRepository $file)
    {
        $this->file = $file;
        $this->file->init(Application::DISK);
    }

    /**
     * @param User|mixed $user
     * @param array $data
     *
     * @return false|Model|Application
     */
    public function create(User $user, array $data)
    {
        \DB::beginTransaction();

        try {

            $files = $this->file->prepare($data);

            $application = new Application($data);
            $application->user()->associate($user);

            if ($application->save() && $this->file->store($application, $files)) {

                \DB::commit();

                event(new ApplicationCreatedEvent($application));

                return $application;
            }

        } catch(\Exception $e) {
            report($e);
        }

        \DB::rollBack();

        return false;
    }

    /**
     * @param Application $application
     * @param array $data
     * @param array $deletedFiles
     *
     * @return false|Model|Application
     */
    public function update(Application $application, array $data, array $deletedFiles = [])
    {
        \DB::beginTransaction();

        try {

            $files = $this->file->prepare($data);

            if ($application->update($data) && $this->file->store($application, $files, $deletedFiles)) {

                \DB::commit();

                return $application;
            }

        } catch (\Exception $e) {
            report($e);
        }

        \DB::rollBack();

        return false;
    }

    /**
     * @param User|mixed $user
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|User[]
     */
    public function getAll(User $user)
    {
        $query = Application::with(['user.profile', 'user.files', 'files']);

        if ($user->isExecutor()) {

            $query
                ->where('status', Application::STATUS_NEW)
                ->orWhereHas('responses', function (Builder $query) use ($user) {
                    $query
                        ->where('selected', true)
                        ->whereHas('user', function (Builder $query) use ($user) {
                            $query->where('id', $user->id);
                        });
                });

        } else {

            $query->whereHas('user', function (Builder $query) use ($user) {
                $query->where('id', $user->id);
            });
        }

        $query->orderBy('created_at', 'desc');

        return $query->paginate();
    }

    /**
     * @param integer $id
     *
     * @return null|Application|Model
     */
    public function getById($id)
    {
        return Application::find($id);
    }

    /**
     * @param Application $application
     * @param User|mixed $user
     *
     * @return bool
     */
    public function addView(Application $application, User $user)
    {
        if ($user->id !== $application->user_id) { // do not calculate owner views

            if (!ApplicationView::whereUserId($user->id)->exists()) { // the user does not exist in views

                $view = new ApplicationView();

                $view->application()->associate($application);
                $view->user()->associate($user);

                return $view->save();
            }
        }

        return true;
    }

    /**
     * @param Carbon $date
     *
     * @return bool
     */
    public function processWithoutResponses(Carbon $date)
    {
        /**@var $query Builder*/
        $query = Application::where('status', Application::STATUS_NEW)
            ->whereDate('created_at', '<', $date)
            ->doesntHave('responses');

        /**@var $application Application*/
        foreach ($query->cursor() as $application) {

            $application->status = Application::STATUS_CANCEL;

            if (!$application->save()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param Carbon $date
     *
     * @return bool
     */
    public function processNotFinished(Carbon $date)
    {
        /**@var $query Builder*/
        $query = Application::whereNotIn('status', [Application::STATUS_COMPLETE, Application::STATUS_CANCEL])
            ->whereHas('responses', function (Builder $query) use ($date) {
                $query->whereDate('created_at', '<', $date);
            });

        /**@var $application Application*/
        foreach ($query->cursor() as $application) {

            $application->status = Application::STATUS_CANCEL;

            if (!$application->save()) {
                return false;
            }
        }

        return true;
    }
}