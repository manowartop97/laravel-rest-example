<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 12/28/17
 * Time: 11:50 AM
 */

namespace App\Repositories\Brand;

use App\Models\Db\Brand;

/**
 * Class BrandRepository
 *
 * @package App\Repositories\Category
 */
class BrandRepository
{
    /**
     * @param string|null $search
     *
     * @return Brand[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll($search = null)
    {
        $query = Brand::whereActive(true);

        if ($search) {
            $query->where('name', 'like', "$search%");
        }

        return $query->get();
    }
}