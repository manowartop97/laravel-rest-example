<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 1/16/18
 * Time: 9:03 AM
 */

namespace App\Repositories\Setting;

use App\Models\Db\Setting;

/**
 * Class SettingRepository
 *
 * @package App\Repositories\Setting
 */
class SettingRepository
{
    /**
     * @return int|null
     */
    public function referralOwnerPrice()
    {
        return ($price = $this->find('referral_owner_price')) ? $price : 0;
    }

    /**
     * @return int|null
     */
    public function referralPrice()
    {
        return ($price = $this->find('referral_price')) ? $price : 0;
    }

    /**
     * @return int|null
     */
    public function applicationResponsePrice()
    {
        return ($price = $this->find('application_response_price')) ? $price : 1;
    }

    /**
     * @return int|null
     */
    public function walletConversion()
    {
        return ($conversion = $this->find('wallet_conversion')) ? $conversion : 1;
    }

    /**
     * @param string $field
     *
     * @return mixed|null
     */
    protected function find($field)
    {
        return ($setting = Setting::first()) ? $setting->$field : null;
    }
}