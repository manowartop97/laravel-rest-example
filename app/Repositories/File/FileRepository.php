<?php

namespace App\Repositories\File;

use App\Models\Db\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileRepository
 * @package App\Repositories\Confirm
 */
class FileRepository
{
    /**
     * @var string
     */
    protected $disk;

    /**
     * @var string
     */
    protected $relation;

    /**
     * @param string $disk
     * @param string $relation
     */
    public function init($disk, $relation = 'files')
    {
        $this->disk = $disk;
        $this->relation = $relation;
    }

    /**
     * @param Model $model
     * @param array $files
     * @param array $deletedFiles
     *
     * @return bool
     */
    public function store(Model $model, array $files, array $deletedFiles = [])
    {
        if (!$this->delete($model, $deletedFiles)) {
            return false;
        }

        foreach ($files as $type => $file) {

            if (\is_array($file)) {

                if (!$this->store($model, $file)) {
                    return false;
                }

            } else {

                if (!$this->save($file, $model, $type)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function prepare(array &$data)
    {
        $fillFiles = function (array $data) use (&$funcFillFiles) {

            $files = [];

            foreach ($data as $field => $file) {

                if (\is_array($file)) {

                    if ($childFiles = $funcFillFiles($file)) {
                        $files[$field] = $childFiles;
                    }

                } else {

                    if ($file instanceof UploadedFile) {
                        $files[$field] = $file;
                    }
                }
            }

            return $files;
        };

        $files = $fillFiles($data);

        $data = array_diff_key($data, $files);

        return $files;
    }

    /**
     * @param UploadedFile $file
     * @param Model $model
     * @param string $type
     *
     * @return bool
     */
    protected function save(UploadedFile $file, Model $model, $type)
    {
        if ($name = $file->store(
            Str::contains($file->getMimeType(), 'image') ? 'images' : 'files', ['disk' => $this->disk])
        ) {

            return $model->{$this->relation}()->create([
                'type' => $type,
                'name' => $name
            ]) ? true : false;
        }

        return false;
    }

    /**
     * @param Model $model
     * @param array $deletedFiles
     *
     * @return bool
     */
    protected function delete(Model $model, array $deletedFiles)
    {
        /** @var $files File[] */
        if ($deletedFiles && ($files = $model->{$this->relation}()->get())) {

            foreach ($files as $file) {

                if (!\in_array($file->id, $deletedFiles, true)) {
                    continue;
                }

                if (!$file->delete()) {
                    return false;
                }

                Storage::disk($this->disk)->delete($file->name);
            }
        }

        return true;
    }
}