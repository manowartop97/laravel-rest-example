<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 12/28/17
 * Time: 11:50 AM
 */

namespace App\Repositories\Category;

use App\Models\Db\Category;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CategoryRepository
 *
 * @package App\Repositories\Category
 */
class CategoryRepository
{
    /**
     * @param string|null $search
     *
     * @return Category[]|Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll($search = null)
    {
        $query= Category::with('types.brands')->whereHas('types', function (Builder $query) use ($search) {

            $query->where('active', true);

            if ($search) {
                $query->where('name', 'like', "$search%");
            }

        })->whereActive(true);

        return $query->get();
    }
}