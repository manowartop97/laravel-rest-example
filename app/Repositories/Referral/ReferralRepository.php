<?php
/**
 * Created by PhpStorm.
 * User: kastiel
 * Date: 1/16/18
 * Time: 10:09 AM
 */

namespace App\Repositories\Referral;

use App\Models\Db\User;
use App\Models\Db\UserProfile;
use App\Repositories\Setting\SettingRepository;
use App\Repositories\User\UserWalletRepository;

/**
 * Class ReferralRepository
 *
 * @package App\Repositories\Referral
 */
class ReferralRepository
{
    /**
     * @var SettingRepository
     */
    protected $setting;

    /**
     * @var UserWalletRepository
     */
    protected $wallet;

    /**
     * ReferralRepository constructor.
     *
     * @param SettingRepository $setting
     * @param UserWalletRepository $wallet
     */
    public function __construct(SettingRepository $setting, UserWalletRepository $wallet)
    {
        $this->wallet = $wallet;
        $this->setting = $setting;
    }

    /**
     * @param User $user
     * @param string|null $referral
     *
     * @return bool
     */
    public function process(User $user, $referral)
    {
        $profile = $referral ? UserProfile::whereReferral($referral)->first() : null;

        if ($profile) {

            //add bonuses for current user
            if (!$this->wallet->payForReferral($user, $this->setting->referralPrice(), $profile->user)) {
                return false;
            }

            //add bonuses for owner
            if (!$this->wallet->payForReferral($profile->user, $this->setting->referralOwnerPrice())) {
                return false;
            }
        }

        return true;
    }
}