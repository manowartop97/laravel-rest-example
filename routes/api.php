<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Dingo\Api\Routing\Router;

/**@var Router $api*/
$api = app(Router::class);

$api->version('v1', ['namespace' => 'App\Api\V1\Http\Controllers'], function (Router $api) {

    $api->group(['namespace' => 'Auth'], function (Router $api) {

        $api->group(['prefix' => 'register'], function (Router $api) {
                $api->post('customer', 'RegisterController@registerCustomer');
                $api->post('master', 'RegisterController@registerMaster');
                $api->post('service', 'RegisterController@registerService');
            });

            $api->group(['prefix' => 'login'], function (Router $api) {
                $api->post('', 'LoginController@login');
                $api->post('send-code', 'LoginController@sendCode');
            });

            $api->post('logout', 'LoginController@logout');
        });

        $api->group(['namespace' => 'User', 'prefix' => 'users'], function (Router $api) {

            $api->get('profile', 'UserController@get');

            $api->group(['prefix' => 'update'], function (Router $api) {
                $api->put('customer', 'UserController@updateCustomer');
            $api->put('master', 'UserController@updateMaster');
            $api->put('service', 'UserController@updateService');
        });

        $api->group(['prefix' => 'change-phone'], function (Router $api) {
            $api->put('', 'ChangePhoneController@change');
            $api->post('send-code', 'ChangePhoneController@sendCode');
        });
    });

    $api->group(['prefix' => 'categories'], function (Router $api) {
        $api->get('', 'CategoryController@getAll');
    });

    $api->group(['prefix' => 'brands'], function (Router $api) {
        $api->get('', 'BrandController@getAll');
    });

    $api->group(['namespace' => 'Application', 'prefix' => 'applications'], function (Router $api) {

        $api->get('', 'ApplicationController@getAll');
        $api->post('create', 'ApplicationController@create');
        $api->get('{id}/show', 'ApplicationController@get');
        $api->put('{id}/update', 'ApplicationController@update');
        $api->put('{id}/change-status', 'ApplicationController@changeStatus');

        $api->group(['prefix' => 'responses'], function (Router $api) {
            $api->post('{id}/create', 'ApplicationResponseController@create');
            $api->put('{id}/update', 'ApplicationResponseController@update');
            $api->put('{id}/confirm', 'ApplicationResponseController@confirm');
        });
    });
});
