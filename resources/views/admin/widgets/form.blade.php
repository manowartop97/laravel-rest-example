<form {!! $attributes !!}>
    <div class="box-body fields-group">

        @foreach((array)$fields as $field)
            {!! $field->render() !!}
        @endforeach

    </div>
    <div class="box-footer">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="btn-group pull-right">
                <button type="submit" class="btn btn-info pull-right">
                    Сохранить
                </button>
            </div>
        </div>
    </div>
</form>