<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateApplicationResponseItemsTable
 */
class CreateApplicationResponseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_response_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('application_response_id');
            $table->string('name');
            $table->integer('price');
            $table->timestamps();
            $table->foreign('application_response_id')->references('id')->on('application_responses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_response_items');
    }
}
