<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDictionariesTable
 */
class CreateDictionariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('active')->default(false);
            $table->timestamps();
        });

        Schema::create('category_types', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->string('name');
            $table->boolean('active')->default(false);
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });

        Schema::create('brands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('active')->default(false);
            $table->timestamps();
        });

        Schema::create('brand_category_type', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_type_id');
            $table->unsignedInteger('brand_id');
            $table->timestamps();
            $table->foreign('category_type_id')->references('id')->on('category_types')->onDelete('cascade');
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand_category_type');
        Schema::dropIfExists('brands');
        Schema::dropIfExists('category_types');
        Schema::dropIfExists('categories');
    }
}
