<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateSettingsTable
 */
class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('referral_owner_price')->nullable()->default(0);
            $table->unsignedInteger('referral_price')->nullable()->default(0);
            $table->unsignedInteger('application_response_price')->nullable()->default(0);
            $table->float('wallet_conversion')->nullable()->default(1);
        });

        DB::table('settings')->insert(['id' => null]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
